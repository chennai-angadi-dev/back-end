const _ = require('lodash')
const BaseController = require('../base/controller')
const { orderService: service } = require('./service')
const { SUCCESS, ERROR } = require('../../libs/constants')
const { apimsgconfig } = require('../../libs/swagger/api_message')

class Controller extends BaseController {
  async createOrder (req, res, next) {
    try {
      const isCreated = await service.createOrder(req.body)
      if (isCreated) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[233], code: '233', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[599], code: '599' })
      }
    } catch (e) {
      next(e)
    }
  }

  async updateCart (req, res, next) {
    try {
      const isCreated = await service.updateCart(req)
      if (isCreated.status === 'success') {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[236], code: '236', data: [] })
      } else
      if (isCreated.status === 'out of stock') {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[242], code: '242', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[599], code: '599' })
      }
    } catch (e) {
      next(e)
    }
  }

  async orderCheckout (req, res, next) {
    try {
      const isCreated = await service.orderCheckout(req)
      if (isCreated) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[232], code: '232', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[599], code: '599' })
      }
    } catch (e) {
      next(e)
    }
  }

  async addToCart (req, res, next) {
    try {
      const data = await service.addToCart(req)
      if (data.status === 'success') {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[202], code: '202', data: data })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: data.status, code: '599' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getOrders (req, res, next) {
    try {
      const result = await service.getAll(req)
      if (!_.isEmpty(result) && result.count > 0) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[200], code: '200', data: result.data, count: result.count })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[206], code: '206' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getOrderById (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.getById(id)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[200], code: '200', data: result })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async updateOrder (req, res, next) {
    try {
      const result = await service.updateOrder(req)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[236], code: '236', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[599], code: '599' })
      }
    } catch (e) {
      next(e)
    }
  }

  async deleteOrder (req, res, next) {
    try {
      const result = await service.deleteOrder(req)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[204], code: '204', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[599], code: '599' })
      }
    } catch (e) {
      next(e)
    }
  }
}
module.exports = Controller
