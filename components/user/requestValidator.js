const joi = require('@hapi/joi')

const read = joi.object().keys()

const list = joi.object().keys()

const create = joi.object().keys({
  mobile_number: joi.string().length(10),
  first_name: joi.string(),
  last_name: joi.string(),
  email: joi.string(),
  gender: joi.string(),
  avatar: joi.string(),
  addressId: joi.string()
})

const update = joi.object().keys({
  mobile_number: joi.string().length(10),
  first_name: joi.string(),
  last_name: joi.string(),
  email: joi.string(),
  gender: joi.string(),
  avatar: joi.string(),
  addressId: joi.string()
})

const remove = joi.object().keys()

const logout = joi.object().keys()

const signUp = joi.object().keys({
  isSocialLogin: joi.boolean(),
  socialid: joi.string(),
  mobile_number: joi.string().when('isSocialLogin', { is: false, then: joi.string().required() }),
  role: joi.string(),
  email: joi.string()
})
const updateProfile = joi.object().keys({
  mobile_number: joi.string(),
  first_name: joi.string(),
  last_name: joi.string(),
  gender: joi.string(),
  avatar: joi.string(),
  email: joi.string(),
  addressId: joi.string()
})

module.exports = {
  list,
  read,
  create,
  update,
  remove,
  signUp,
  logout,
  updateProfile
}
