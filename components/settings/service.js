const Setting = require('./schema')
const { errLogger } = require('../../config/logger')

class Service {
  async createSetting (req) {
    var documentcount = await Setting.countDocuments().lean()
    if (documentcount <= 0) {
      const data = new Setting(req.body)
      return data.save()
    } else {
      return false
    }
  }

  async getSettingDetails () {
    return Setting.find().sort({ createdAt: -1 }).lean().catch((e) => {
      errLogger.error({ method: 'Setting-getAll', message: e.message })
    })
  }

  async updateSettings (id, updateParams) {
    return Setting.updateOne(id, updateParams).catch((e) => {
      errLogger.error({ method: 'Setting-update', message: e.message })
    })
  }
}

const settingService = new Service()

module.exports = { Service, settingService }
