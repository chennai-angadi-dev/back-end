module.exports = {
  required: [
    'name',
    'productId',
    'categoryId',
    'description',
    'type',
    'discount_value',
    'valid_from',
    'valid_to',
    'minimum_order_amount',
    'maximum_discount_amount',
    'image',
    'status'
  ],
  create: {
    required: [
      'name',
      'productId',
      'categoryId',
      'description',
      'type',
      'discount_value',
      'valid_from',
      'valid_to',
      'minimum_order_amount',
      'maximum_discount_amount',
      'image',
      'status'
    ],
    properties: {
      name: {
        type: 'string'
      },
      categoryId: {
        type: 'string'
      },
      productId: {
        type: 'string'
      },
      description: {
        type: 'string'
      },
      type: {
        type: 'string',
        default: 'Select type',
        enum: ['Flat', '%']
      },
      discount_value: {
        type: 'number'
      },
      valid_from: {
        type: 'string',
        format: 'date'
      },
      valid_to: {
        type: 'string',
        format: 'date'
      },
      minimum_order_amount: {
        type: 'number'
      },
      maximum_discount_amount: {
        type: 'number'
      },
      image: {
        type: 'string'
      },
      status: {
        type: 'string',
        default: 'ACTIVE',
        enum: ['ACTIVE', 'INACTIVE', 'DELETED']
      }
    }
  },
  update: {
    properties: {
      name: {
        type: 'string'
      },
      categoryId: {
        type: 'string'
      },
      productId: {
        type: 'string'
      },
      description: {
        type: 'string'
      },
      type: {
        type: 'string',
        default: 'Select type',
        enum: ['Flat', '%']
      },
      discount_value: {
        type: 'number'
      },
      valid_from: {
        type: 'string',
        format: 'date'
      },
      valid_to: {
        type: 'string',
        format: 'date'
      },
      minimum_order_amount: {
        type: 'number'
      },
      maximum_discount_amount: {
        type: 'number'
      },
      image: {
        type: 'string'
      },
      status: {
        type: 'string',
        default: 'ACTIVE',
        enum: ['ACTIVE', 'INACTIVE', 'DELETED']
      }
    }
  },
  properties: {
    name: {
      type: 'string'
    },
    categoryId: {
      type: 'string'
    },
    productId: {
      type: 'string'
    },
    description: {
      type: 'string'
    },
    type: {
      type: 'string',
      default: 'Select type',
      enum: ['Flat', '%']
    },
    discount_value: {
      type: 'Number'
    },
    valid_from: {
      type: 'date'
    },
    valid_to: {
      type: 'date'
    },
    minimum_order_amount: {
      type: 'number'
    },
    maximum_discount_amount: {
      type: 'number'
    },
    image: {
      type: 'string'
    },
    status: {
      type: 'string',
      default: 'ACTIVE',
      enum: ['ACTIVE', 'INACTIVE', 'DELETED']
    }
  }
}
