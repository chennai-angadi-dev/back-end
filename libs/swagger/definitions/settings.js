module.exports = {
  required: [
    'terms',
    'privacy',
    'about_us',
    'image',
    'email_us'
  ],
  create: {
    required: [
      'terms',
      'privacy',
      'about_us',
      'image',
      'email_us'
    ],
    properties: {
      terms: {
        type: 'string'
      },
      privacy: {
        type: 'string'
      },
      about_us: {
        type: 'string'
      },
      image: {
        type: 'string'
      },
      email_us: {
        type: 'string'
      }
    }
  },
  update: {
    properties: {
      terms: {
        type: 'string'
      },
      privacy: {
        type: 'string'
      },
      about_us: {
        type: 'string'
      },
      image: {
        type: 'string'
      },
      email_us: {
        type: 'string'
      }
    }
  },
  properties: {
    terms: {
      type: 'string'
    },
    privacy: {
      type: 'string'
    },
    about_us: {
      type: 'string'
    },
    image: {
      type: 'string'
    },
    email_us: {
      type: 'string',
      trim: true
    }
  }
}
