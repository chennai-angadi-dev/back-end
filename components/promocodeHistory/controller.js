const _ = require('lodash')
const BaseController = require('../base/controller')
const { promocodeService: service } = require('./service')
const { SUCCESS, ERROR } = require('../../libs/constants')
const { apimsgconfig } = require('../../libs/swagger/api_message')

class Controller extends BaseController {
  async createPromocodehistory (req, res, next) {
    try {
      const result = await service.createPromocode(req)
      if (result.status === 'success') {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[234], code: '202', data: result.data })
      } else if (result.status === 'promocode already applied') {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[465], code: '465' })
      } if (result.status === 'order amount lower than required amount') {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[466], code: '466' })
      } if (result.status === 'invalid promocode') {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[463], code: '463' })
      } if (result.status === 'invalid promocode expired') {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[464], code: '464' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getPromocodeHistories (req, res, next) {
    try {
      const result = await service.getAll(req)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[200], code: '200', data: [result] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async removePromocodehistory (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.removePromocode({ _id: id }, { status: 'REDEEM' })
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[235], code: '204', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }
}

module.exports = Controller
