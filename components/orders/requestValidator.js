const joi = require('@hapi/joi')

const read = joi.object().keys()

const list = joi.object().keys()

const checkout = joi.object().keys({
  product_count: joi.string(),
  productId: joi.string()
})

const create = joi.object().keys({
  delivery_charge: joi.string(),
  package_charge: joi.string(),
  tax_amount: joi.string(),
  promocodeId: joi.string(),
  total_price: joi.string(),
  order_price: joi.string(),
  total_products: joi.string(),
  addressId: joi.string(),
  products: joi.array()
    .items({
      product_count: joi.string(),
      productId: joi.string(),
      price_range: joi.array()
        .items({
          label: joi.string(),
          value: joi.string(),
          stock: joi.string(),
          is_selected: joi.string().valid('1', '0')
        })
    })
})

const updatecart = joi.object().keys({
  product_count: joi.number()
    .required(),
  productId: joi.string()
    .required(),
  orderId: joi.string().required()
})

const update = joi.object().keys({
  delivery_charge: joi.string(),
  package_charge: joi.string(),
  tax_amount: joi.string(),
  promocodeId: joi.string(),
  total_price: joi.string(),
  order_price: joi.string(),
  total_products: joi.string(),
  addressId: joi.string(),
  products: joi.array()
    .items({
      product_count: joi.string()
        .required(),
      productId: joi.string()
        .required(),
      price_range: joi.array()
        .items({
          label: joi.string(),
          value: joi.string(),
          is_selected: joi.string().valid('1', '0')
        })
    })
})

const remove = joi.object().keys({
  productId: joi.string()
})

module.exports = {
  list,
  read,
  updatecart,
  create,
  update,
  remove,
  checkout
}
