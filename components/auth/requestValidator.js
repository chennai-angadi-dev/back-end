const joi = require('@hapi/joi')

const login = joi.object().keys({
  device_token: joi.string(),
  device_type: joi.string(),
  mobile_number: joi.string().when('isSocialLogin', { is: false, then: joi.string().required() }),
  isSocialLogin: joi.boolean(),
  socialid: joi.string(),
  username: joi.string()
})

module.exports = {
  login
}
