module.exports = {
  HEADER: {
    CONTENT_TYPE: 'application/json'
  },
  ERROR: {
    CODE: 500,
    MSG: 'error'
  },
  SUCCESS: {
    CODE: 200,
    MSG: 'ok'
  },
  MSG: {
    HAS_RECORD: 'record(s) found'
  },
  BAD_REQUEST: 'Bad request',
  UNAUTHORIZED: 'Unauthorized',
  NO_RECORD: 'No records found',
  TRY_AGAIN: 'Try again',

  STATUS: {
    ALL: 'ALL',
    ACTIVE: 'ACTIVE',
    INACTIVE: 'INACTIVE',
    DELETE: 'DELETED',
    VERIFIED: 'VERIFIED',
    UN_VERIFIED: 'UN_VERIFIED',
    EXPIRED: 'EXPIRED',
    SUSPENDED: 'SUSPENDED'
  },
  ROLES: {
    ADMIN: 'ADMIN',
    OWNER: 'OWNER',
    MANAGER: 'MANAGER',
    CO_ORDINATOR: 'CO_ORDINATOR',
    STAFF: 'STAFF',
    EXECUTIVE: 'EXECUTIVE',
    ADMIN_MANAGER: 'ADMIN_MANAGER'
  },
  IS_STATIC_OTP: process.env.IS_STATIC_OTP,
  STATIC_OTP: process.env.STATIC_OTP,
  STATIC_PASSWORD: process.env.STATIC_PASSWORD,
  OTP_TYPE: {
    LOGIN: 'LOGIN',
    SIGN_UP: 'SIGN_UP'
  },
  ALLOWED_MIME_TYPE: ['image/jpeg', 'image/png', 'image/jpg', 'image/JPEG', 'image/PNG', 'image/JPG', 'application/pdf', 'image/bmp', 'image/gif', 'application/PDF', 'image/BMP', 'image/GIF', 'video/mp4', 'video/ogg'],
  ERR_FILE_TYPE: 'File type not allowed'
}
