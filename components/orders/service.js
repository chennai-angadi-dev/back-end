const Order = require('./schema')
const { errLogger } = require('../../config/logger')
const { STATUS } = require('../../libs/constants')
const Products = require('../product/schema')
const Address = require('../address/schema')

class Service {
  async createOrder (req) {
    const productPriceRange = await Products.findOne({ productId: req.productId })
    if (productPriceRange) {
      var priceRange = productPriceRange.price_range
      if (priceRange) {
        const data = new Order(req)
        return data.save().catch((e) => {
          errLogger.error({ method: 'create-order', message: e.message })
        })
      }
    }
  }

  async orderCheckout (req) {
    const pendingOrder = await Order.findOne({ orderId: req._id })
    if (pendingOrder) {
      var products = pendingOrder.products
      var orderPrice = 0
      for (var i = 0; i < products.length; i++) {
        var selectedPriceRange = this.findSelectedPriceRange(products[i].price_range)
        if (selectedPriceRange) {
          orderPrice = orderPrice + (selectedPriceRange.stock * selectedPriceRange.value)
        }
      }
      var totCost = orderPrice + pendingOrder.tax_amount + pendingOrder.delivery_charge + pendingOrder.package_charge
      products.push(req.body)
      const updatedOrder = await Order.updateOne({ _id: pendingOrder._id }, { status: 'ORDER_PLACED', products: products, order_price: orderPrice, total_price: totCost }).catch((e) => {
        errLogger.error({ method: 'Order-create', message: e.message })
      })
      if (updatedOrder.nModified === 1) { return true }
    }
    return false
  }

  async updateCart (req) {
    if (req.user.id) {
      const update = await Order.findOne({ _id: req.body.orderId }).lean()
      if (update) {
        var products = update.products
        var existingProdInOrder = this.findInArrayObj(req.body.productId, products)
        if (existingProdInOrder === null) {
          var productData = await Products.findById(req.body.productId).lean()
          if (productData) {
            var selectedPriceRange = this.findSelectedPriceRange(productData.price_range)
            if (selectedPriceRange != null) {
              if (selectedPriceRange.stock < productData.total_stock) {
                products.push(req.body)
                const updatedOrder = await Order.updateOne({ _id: update._id }, { products: products, total_products: products.length })
                if (updatedOrder.nModified === 1) { return { data: {}, status: 'success' } } else {
                  return { data: {}, status: 'mongoDB error' }
                }
              } else {
                return { data: [], status: 'out of stock' }
              }
            }
          } else {
            return { data: [], status: 'invalid price range' }
          }
        } else {
          return { data: [], status: 'invalid product id' }
        }
      } else {
        return { data: [], status: 'invalid request' }
      }
    } else {
      var productData1 = await Products.findById(req.body.productId).lean()
      var addressId = null
      var address = await Address.findOne({ userId: req.user.id, is_default: '1' }).lean()
      if (!address) {
        address = await Address.findOne({ userId: req.user.id }).lean()
        if (!address) {
          return { data: [], status: 'invalid' }
        }
      }
      if (address) { addressId = address._id }
      if (productData1) {
        var productsArray = []
        const value = productsArray.push({ productId: req.body.productId, price_range: productData1.price_range, productCount: req.body.product_count })
        if (value) {
          var order = { userId: req.user.id, products: productsArray, total_products: 1, status: STATUS.PENDING, delivery_charge: 0, package_charge: 0, tax_amount: 0, order_price: 0, total_price: 0, addressId: addressId }
          const data = new Order(order)
          const createdData = await data.save().catch((e) => {
            errLogger.error({ method: 'Order-save', message: e.message })
          })
          return { data: createdData || {}, status: createdData ? 'success' : 'invalid request' }
        } else {
          return { data: [], status: 'invalid product id' }
        }
      }
    }
  }

  findSelectedPriceRange (priceRange) {
    for (var i = 0; i < priceRange.length; i++) {
      if (priceRange[i].is_selected === '1') {
        return priceRange[i]
      }
    }
    return null
  }

  findInArrayObj (prodId, myArray) {
    for (var i = 0; i < myArray.length; i++) {
      if (myArray[i].productId === prodId) {
        return myArray[i]
      }
    }
    return null
  }

  removeProductFromArray (prodId, myArray) {
    var products = []
    for (var i = 0; i < myArray.length; i++) {
      if (myArray[i].productId !== prodId) {
        products.push(myArray[i])
      }
    }
    return products
  }

  async getAll (req) {
    const reqQuery = req.query
    const limit = Number(
      reqQuery && reqQuery.limit ? reqQuery.limit : process.env.PAGE_LIMIT
    )
    const pageNo = Number(reqQuery && reqQuery.page ? limit * reqQuery.page - limit : 0)
    const sortField = reqQuery && reqQuery.sort ? reqQuery.sort : 'createdAt'
    const order = reqQuery && reqQuery.order && reqQuery.order === 'asc' ? 1 : -1
    if (req.user.id) {
      const data = await Order.find().select('products total_products delivery_charge package_charge tax_amount order_price total_price addressId ').lean()
        .populate({
          path: 'promocodeId',
          select: 'code'
        })
        .populate({
          path: 'addressId',
          select: 'flat_no pincode city landmark area is_default street'
        }).populate({
          path: 'order',
          options: {
            sort: { [`${sortField}`]: order },
            skip: pageNo,
            limit: limit
          }
        })
        .lean().catch((e) => {
          errLogger.error({ method: 'Order-getAll', message: e.message })
        })
      return { data: data, count: data ? data.length : 0 }
    }
  }

  async getById (id) {
    return Order.findById(id)
      .populate({
        path: 'promocodeId',
        select: 'code'
      })
      .populate({
        path: 'addressId',
        select: 'flat_no pincode city landmark area is_default street'
      })
      .lean().catch((e) => {
        errLogger.error({ method: 'Order-getById', message: e.message })
      })
  }

  async updateOrder (id, updateParams) {
    return Order
      .updateOne(id, updateParams).catch((e) => {
        errLogger.error({ method: 'order-getById', message: e.message })
      })
  }

  async deleteOrder (req) {
    const pendingOrder = await Order.findOne({ status: 'PENDING' })
    if (pendingOrder) {
      var products = pendingOrder.products
      products = this.removeProductFromArray(req.body.productId, products)
      const updatedOrder = await Order.updateOne({ _id: pendingOrder._id }, { products: products, total_products: products.length })
      if (updatedOrder.nModified === 1) { return { data: {}, status: 'success' } } else {
        return { data: {}, status: 'mongoDB error' }
      }
    } else {
      return { data: [], status: 'invalid request' }
    }
  }
}

const orderService = new Service()

module.exports = { Service, orderService }
