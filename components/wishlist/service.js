const Wishlist = require('./schema')
const { errLogger } = require('../../config/logger')
const _ = require('lodash')

class Service {
  async addOrRemoveWishlist (params) {
    const count = await Wishlist.countDocuments(params)
    if (count === 0) {
      const data = new Wishlist(params)
      const savedData = await data.save().catch((e) => {
        errLogger.error({ method: 'Wishlist-save', message: e.message })
      })
      if (!_.isEmpty(savedData) && savedData.id) {
        return { type: 'Add', success: true }
      } else {
        return { type: 'Add', success: false }
      }
    } else {
      const removeData = await Wishlist.remove(params).catch((e) => { errLogger.error({ method: 'Wishlist-remove', message: e.message }) })
      if (removeData === 0) {
        return { type: 'Remove', success: false }
      } else {
        return { type: 'Remove', success: true }
      }
    }
  }

  async getWishlistAndCount (req) {
    const reqQuery = req.query
    const limit = Number(
      reqQuery && reqQuery.limit ? reqQuery.limit : process.env.PAGE_LIMIT
    )
    const pageNo = Number(reqQuery && reqQuery.page ? limit * reqQuery.page - limit : 0)
    const sortField = reqQuery && reqQuery.sort ? reqQuery.sort : 'createdAt'
    const order = reqQuery && reqQuery.order && reqQuery.order === 'asc' ? 1 : -1
    const match = { $and: [] }

    if (reqQuery.product) {
      match.$and.push({ product: { $regex: reqQuery.product, $options: 'i' } })
    } else {
      match.$and.push({ product: { $ne: !null } })
    }
    match.$and.push({ userId: req.user.id })

    var findQuery = null
    if (reqQuery.product) {
      findQuery = { productId: reqQuery.product, userId: req.user.id }
    } else { findQuery = { userId: req.user.id } }
    const wishListData = await (await Wishlist.find(findQuery).select('productId').populate({
      path: 'productId',
      select: 'name categoryId subcategoryId brandId description image price price_range rate_per total_stock alert_stock status'
    }).populate({
      path: 'productId',
      options: {
        sort: { [`${sortField}`]: order },
        skip: pageNo,
        limit: limit
      }
    }).lean())
    return { data: wishListData, count: wishListData ? wishListData.length : 0 }
  }

  async getAll () {
    return Wishlist.find()
      .populate({
        path: 'productId',
        select: 'name'
      })
      .select('productId')
      .sort({ createdAt: -1 }).lean().catch((e) => {
        errLogger.error({ method: 'Wishlist-getAll', message: e.message })
      })
  }

  async getById (id) {
    return Wishlist.findById(id)
      .populate({
        path: 'productId',
        select: 'name'
      })
      .select('productId')
      .catch((e) => {
        errLogger.error({ method: 'Wishlist-getById', message: e.message })
      })
  }
}

const wishlistService = new Service()

module.exports = { Service, wishlistService }
