const mongoose = require('mongoose')
const Schema = mongoose.Schema

const schema = new mongoose.Schema(
  {
    name: {
      type: String,
      index: true,
      trim: true,
      unique: true,
      required: [true, 'Name is required.']
    },
    categoryId: {
      type: Schema.Types.ObjectId,
      ref: 'categories',
      required: [true, 'category is required.']
    },
    subcategoryId: {
      type: Schema.Types.ObjectId,
      ref: 'subcategories',
      required: [true, ' subcategory is required.']
    },
    price: {
      type: String
    },
    description: {
      type: String
    },
    image: {
      type: Array
    },
    price_range: {
      type: Array
    },
    brandId: {
      type: Schema.Types.ObjectId,
      ref: 'brands',
      required: [true, ' brand is required.']
    },
    rate_per: {
      type: String,
      default: 'Select rate per',
      enum: ['litre', 'pack', 'Kg', 'piece', 'gram', 'millitre']
    },
    total_stock: {
      type: String
    },
    alert_stock: {
      type: String
    },
    status: {
      type: String,
      default: 'ACTIVE',
      enum: ['ACTIVE', 'ACTIVE', 'DELETED ']
    }
  },
  { timestamps: true, versionKey: false }
)

const Products = mongoose.model('products', schema)
module.exports = Products
