const mongoose = require('mongoose')
const Schema = mongoose.Schema
const schema = new mongoose.Schema(
  {
    orderId: {
      type: Schema.Types.ObjectId,
      ref: 'checkouts',
      required: [true, 'checkout is required.']
    },
    order_status: {
      type: String,
      default: 'ORDER_PLACED',
      enum: ['ORDER_PLACED', 'CONFIRMED', 'ORDER_PROCESSED', 'READY_TO_PICKUP', 'DELIVERY_IN_PROGRESS', 'DELIVERED', 'CANCELLED']
    }
  },
  { timestamps: true, versionKey: false }
)

const OrderHistory = mongoose.model('orderhistories', schema)
module.exports = OrderHistory
