module.exports = {
  required: [
    'name',
    'categoryId'
  ],
  create: {
    required: [
      'name',
      'categoryId'
    ],
    properties: {
      name: {
        type: 'string',
        index: true,
        trim: true,
        unique: true,
        required: [true, 'Name is required.']
      },
      status: {
        type: 'string',
        default: 'ACTIVE',
        enum: ['ACTIVE', 'INACTIVE', 'DELETED ']
      },
      categoryId: {
        type: 'string',
        trim: true
      }
    }
  },
  update: {
    properties: {
      name: {
        type: 'string',
        index: true,
        trim: true,
        unique: true,
        required: [true, 'Name is required.']
      },
      status: {
        type: 'string',
        default: 'ACTIVE',
        enum: ['ACTIVE', 'INACTIVE', 'DELETED ']
      },
      categoryId: {
        type: 'string',
        trim: true
      }
    }
  },
  properties: {
    id: {
      type: String,
      trim: true
    },
    name: {
      type: 'string',
      index: true,
      trim: true,
      unique: true,
      required: [true, 'Name is required.']
    },
    status: {
      type: 'string',
      default: 'ACTIVE',
      enum: ['ACTIVE', 'INACTIVE', 'DELETED ']
    },
    categoryId: {
      type: 'string',
      trim: true
    }
  }
}
