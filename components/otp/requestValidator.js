
const joi = require('@hapi/joi')

const generateOtp = joi.object().keys({
  mobileNumber: joi.string().required(),
  type: joi.string().valid('LOGIN', 'SIGN_UP').required()
})

const verifyOtp = joi.object().keys({
  mobile_number: joi.string().length(10).required(),
  type: joi.string().valid('LOGIN', 'SIGN_UP', 'SIGN_IN').required(),
  code: joi.string().min(6).max(6).required()
})
const sendOtp = joi.object().keys({
  mobile_number: joi.string().required(),
  type: joi.string().valid('LOGIN', 'SIGN_UP', 'SIGN_IN').required()
})
const list = joi.object().keys()

module.exports = {
  generateOtp,
  verifyOtp,
  list,
  sendOtp
}
