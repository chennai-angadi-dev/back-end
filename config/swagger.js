const definitions = require('../libs/swagger/definitions/index')

module.exports = {
  swagger: '2.0',
  info: {
    version: '1.0.0',
    title: 'Chennai Angadi Application',
    description: ''
  },
  host: `${process.env.HOST}`,
  basePath: '/api/v1',
  tags: [
    {
      name: 'Authentication',
      description: 'Generate Auth Token to access resources'
    },
    {
      name: 'Otp',
      description: 'API for Otp'
    },
    {
      name: 'User',
      description: 'API for Users'
    },
    {
      name: 'Uploads',
      description: 'API for Uploads'
    },
    {
      name: 'Category',
      description: 'API for Category'
    },
    {
      name: 'wishlist',
      description: 'API for wishlist'
    },
    {
      name: 'brands',
      description: 'API for brands'
    },
    {
      name: 'Subcategory',
      description: 'API for Subcategory'
    },
    {
      name: 'Promocode',
      description: 'API for promocode'
    },
    {
      name: 'Offers',
      description: 'API for offers'
    },
    {
      name: 'Product',
      description: 'API for product'
    },
    {
      name: 'Uploads',
      description: 'API for Uploads'
    },
    {
      name: 'Notifications',
      description: 'API for notifications'
    },
    {
      name: 'Address',
      description: 'API for address'
    }
  ],
  schemes: `${process.env.SCHEMES}`.split(','),
  consumes: ['application/json'],
  produces: ['application/json'],
  securityDefinitions: {
    auth: {
      type: 'apiKey',
      description: 'JWT authorization of an API',
      name: 'Authorization',
      in: 'header'
    }
  },
  security: [
    { auth: [] }
  ],
  paths: {
    '/auth/authentication': {
      post: {
        tags: ['Authentication'],
        description: 'Create new Auth Token to access resources',
        parameters: [
          {
            name: 'user',
            in: 'body',
            schema: {
              $ref: '#/definitions/Auth/authenticate'
            }
          }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/user': {
      post: {
        tags: ['User'],
        summary: 'Add a new user',
        description: '',
        operationId: 'addUser',
        parameters: [
          {
            name: 'user',
            in: 'body',
            schema: {
              $ref: '#/definitions/User/create'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      get: {
        tags: ['User'],
        summary: 'Find users',
        description: '',
        operationId: 'findUsers',
        parameters: [
          {
            name: 'status',
            in: 'query',
            description: 'Return Based on user status',
            type: 'string',
            enum: ['ACTIVE', 'INACTIVE', 'DELETED']
          },
          {
            name: 'email',
            in: 'query',
            description: 'Return Based on user email',
            type: 'string'
          },
          {
            name: 'mobile_number',
            in: 'query',
            description: 'Return Based on user mobile',
            type: 'string'
          },
          {
            name: 'page',
            in: 'query',
            type: 'number',
            default: 1
          },
          {
            name: 'limit',
            in: 'query',
            type: 'number',
            minimum: 1,
            maximum: 100,
            default: 20
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/user/{id}': {
      get: {
        tags: ['User'],
        summary: 'Find user by ID',
        description: 'Returns a single user',
        operationId: 'getUserById',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Return single user data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      '/user/profile': {
        get: {
          tags: ['User'],
          summary: 'Find user profile by token',
          description: 'Returns a single user profile',
          operationId: 'getprofile',
          parameters: [
            {
              name: 'id',
              in: 'path',
              description: 'Return single user data profile',
              required: true,
              type: 'string'
            },
            { $ref: '#/definitions/Headers/content_type' }
          ],
          responses: {
            200: {
              $ref: '#/definitions/Response/200'
            },
            400: {
              $ref: '#/definitions/Response/400'
            },
            401: {
              $ref: '#/definitions/Response/401'
            },
            404: {
              $ref: '#/definitions/Response/404'
            },
            500: {
              $ref: '#/definitions/Response/500'
            }
          }
        },
        put: {
          tags: ['User'],
          summary: 'Update an existing user profile',
          description: '',
          operationId: 'updateprofile',
          parameters: [
            {
              name: 'id',
              in: 'path',
              description: 'ID of user that needs to be updated',
              required: true,
              type: 'string'
            },
            {
              in: 'body',
              name: 'user',
              description: 'Updated user profile object',
              required: true,
              schema: {
                $ref: '#/definitions/User/update'
              }
            },
            { $ref: '#/definitions/Headers/content_type' }
          ],
          responses: {
            200: {
              $ref: '#/definitions/Response/200'
            },
            400: {
              $ref: '#/definitions/Response/400'
            },
            401: {
              $ref: '#/definitions/Response/401'
            },
            404: {
              $ref: '#/definitions/Response/404'
            },
            500: {
              $ref: '#/definitions/Response/500'
            }
          }
        }
      },
      put: {
        tags: ['User'],
        summary: 'Update an existing user',
        description: '',
        operationId: 'updateUser',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'ID of user that needs to be updated',
            required: true,
            type: 'string'
          },
          {
            in: 'body',
            name: 'user',
            description: 'Updated user object',
            required: true,
            schema: {
              $ref: '#/definitions/User/update'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      delete: {
        tags: ['User'],
        summary: 'Delete User by ID',
        description: 'Delete single User data',
        operationId: 'deleteUser',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Delete single User data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/user/signUp': {
      post: {
        tags: ['User'],
        summary: 'Add a new user',
        description: 'Signup newcuser',
        operationId: 'addUser',
        parameters: [
          {
            name: 'user',
            in: 'body',
            schema: {
              $ref: '#/definitions/User/signUp'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/user/logout': {
      get: {
        tags: ['User'],
        summary: 'logout a user',
        description: 'logout newuser',
        operationId: 'logoutUser',
        parameters: [
          {
            name: 'user',
            in: 'body',
            schema: {
              $ref: '#/definitions/User/logout'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },

    '/otp/generate': {
      post: {
        tags: ['Otp'],
        summary: 'Generate Otp',
        description: 'Send OTP to user from compliance app system',
        parameters: [
          {
            name: 'user',
            in: 'body',
            schema: {
              $ref: '#/definitions/Otp/generateOtp'
            }
          }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/otp/verifyOtp': {
      post: {
        tags: ['Otp'],
        summary: 'Verify OTP',
        description: 'Verify OTP in compliance app System',
        parameters: [
          {
            name: 'user',
            in: 'body',
            schema: {
              $ref: '#/definitions/Otp/verifyOtp'
            }
          }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/otp/sendOtp': {
      post: {
        tags: ['Otp'],
        summary: 'send OTP',
        description: 'send OTP in chennai angadi app System',
        parameters: [
          {
            name: 'user',
            in: 'body',
            schema: {
              $ref: '#/definitions/Otp/sendOtp'
            }
          }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },

    '/orders/checkout': {
      post: {
        tags: ['Order'],
        summary: 'Add a new order',
        description: '',
        operationId: 'createOrder',
        parameters: [
          {
            name: 'order',
            in: 'body',
            schema: {
              $ref: '#/definitions/Orders/create'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/orders': {
      post: {
        tags: ['Order'],
        summary: 'Add a new order',
        description: '',
        operationId: 'createOrder',
        parameters: [
          {
            name: 'order',
            in: 'body',
            schema: {
              $ref: '#/definitions/Orders/updatecart'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      get: {
        tags: ['Order'],
        summary: 'Find order',
        description: '',
        operationId: 'getorders',
        parameters: [
          {
            name: 'status',
            in: 'query',
            description: 'Return Based on user status',
            type: 'string',
            enum: ['ACTIVE', 'INACTIVE', 'DELETED']
          },
          {
            name: 'page',
            in: 'query',
            type: 'number',
            default: 1
          },
          {
            name: 'limit',
            in: 'query',
            type: 'number',
            minimum: 1,
            maximum: 100,
            default: 20
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      delete: {
        tags: ['Order'],
        summary: 'Delete order by ID',
        description: 'Delete single order data',
        operationId: 'deleteOrder',
        parameters: [
          {
            in: 'body',
            name: 'user',
            description: 'Updated order object',
            required: true,
            schema: {
              $ref: '#/definitions/Orders/delete'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/orders/{id}': {
      get: {
        tags: ['Order'],
        summary: 'Find order by ID',
        description: 'Returns a single order',
        operationId: 'getOrderById',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Return single order data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      put: {
        tags: ['Order'],
        summary: 'Update an existing order',
        description: '',
        operationId: 'updateOrder',
        parameters: [
          {
            in: 'body',
            name: 'user',
            description: 'Updated order object',
            required: true,
            schema: {
              $ref: '#/definitions/Orders/update'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }

    },
    '/product': {
      post: {
        tags: ['Product'],
        summary: 'Add a new product',
        description: '',
        operationId: 'createProduct',
        parameters: [
          {
            name: 'product',
            in: 'body',
            schema: {
              $ref: '#/definitions/Product/create'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      get: {
        tags: ['Product'],
        summary: 'Find product',
        description: '',
        operationId: 'getProducts',
        parameters: [
          {
            name: 'status',
            in: 'query',
            description: 'Return Based on user status',
            type: 'string',
            enum: ['ACTIVE', 'INACTIVE', 'DELETED']
          },
          {
            name: 'page',
            in: 'query',
            type: 'number',
            default: 1
          },
          {
            name: 'category',
            in: 'query',
            type: 'array',
            items: {
              type: 'string'
            }
          },
          {
            name: 'brands',
            in: 'query',
            type: 'array',
            items: {
              type: 'string'
            }
          },
          {
            name: 'price',
            in: 'query',
            type: 'array',
            items: {
              type: 'string'
            }
          },
          {
            name: 'offer',
            in: 'query',
            type: 'array',
            items: {
              type: 'string'
            }
          },
          {
            name: 'limit',
            in: 'query',
            type: 'number',
            minimum: 1,
            maximum: 100,
            default: 20
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/product/{id}': {
      get: {
        tags: ['Product'],
        summary: 'Find product by ID',
        description: 'Returns a single product',
        operationId: 'getProductById',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Return single product data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      put: {
        tags: ['Product'],
        summary: 'Update an existing product',
        description: '',
        operationId: 'updateProduct',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'ID of product that needs to be updated',
            required: true,
            type: 'string'
          },
          {
            in: 'body',
            name: 'user',
            description: 'Updated product object',
            required: true,
            schema: {
              $ref: '#/definitions/Product/update'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      delete: {
        tags: ['Product'],
        summary: 'Delete product by ID',
        description: 'Delete single product data',
        operationId: 'deleteProduct',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Delete single product data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/category': {
      post: {
        tags: ['Category'],
        summary: 'Add a new category',
        description: '',
        operationId: 'createCategory',
        parameters: [
          {
            name: 'category',
            in: 'body',
            schema: {
              $ref: '#/definitions/Category/create'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      get: {
        tags: ['Category'],
        summary: 'Find category',
        description: '',
        operationId: 'getCategory',
        parameters: [
          {
            name: 'status',
            in: 'query',
            description: 'Return Based on user status',
            type: 'string',
            enum: ['ACTIVE', 'INACTIVE', 'DELETED']
          },
          {
            name: 'page',
            in: 'query',
            type: 'number',
            default: 1
          },
          {
            name: 'limit',
            in: 'query',
            type: 'number',
            minimum: 1,
            maximum: 100,
            default: 20
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/category/{id}': {
      get: {
        tags: ['Category'],
        summary: 'Find category by ID',
        description: 'Returns a single category',
        operationId: 'getCategoryById',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Return single category data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      put: {
        tags: ['Category'],
        summary: 'Update an existing category',
        description: '',
        operationId: 'updateCategory',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'ID of category that needs to be updated',
            required: true,
            type: 'string'
          },
          {
            in: 'body',
            name: 'user',
            description: 'Updated category object',
            required: true,
            schema: {
              $ref: '#/definitions/Category/update'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      delete: {
        tags: ['Category'],
        summary: 'Delete category by ID',
        description: 'Delete single category data',
        operationId: 'deleteCategory',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Delete single category data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/settings': {
      post: {
        tags: ['Settings'],
        summary: 'Add a new settings',
        description: '',
        operationId: 'createSettings',
        parameters: [
          {
            name: 'settings',
            in: 'body',
            schema: {
              $ref: '#/definitions/Settings/create'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      get: {
        tags: ['Settings'],
        summary: 'Find settings',
        description: '',
        operationId: 'getSettings',
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/settings/{id}': {
      put: {
        tags: ['Settings'],
        summary: 'Update an existing settings',
        description: '',
        operationId: 'updateSettings',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'ID of settings that needs to be updated',
            required: true,
            type: 'string'
          },
          {
            in: 'body',
            name: 'user',
            description: 'Updated settings object',
            required: true,
            schema: {
              $ref: '#/definitions/Settings/update'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/subcategory': {
      post: {
        tags: ['Subcategory'],
        summary: 'Add a new subcategory',
        description: '',
        operationId: 'createSubcategory',
        parameters: [
          {
            name: 'subcategory',
            in: 'body',
            schema: {
              $ref: '#/definitions/Subcategory/create'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      get: {
        tags: ['Subcategory'],
        summary: 'Find subcategory',
        description: '',
        operationId: 'getSubcategories',
        parameters: [
          {
            name: 'status',
            in: 'query',
            description: 'Return Based on user status',
            type: 'string',
            enum: ['ACTIVE', 'INACTIVE', 'DELETED']
          },
          {
            name: 'page',
            in: 'query',
            type: 'number',
            default: 1
          },
          {
            name: 'limit',
            in: 'query',
            type: 'number',
            minimum: 1,
            maximum: 100,
            default: 20
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/subcategory/{id}': {
      get: {
        tags: ['Subcategory'],
        summary: 'Find subcategory by ID',
        description: 'Returns a single subcategory',
        operationId: 'getSubcategoryById',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Return single subcategory data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      put: {
        tags: ['Subcategory'],
        summary: 'Update an existing subcategory',
        description: '',
        operationId: 'updateSubcategory',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'ID of subcategory that needs to be updated',
            required: true,
            type: 'string'
          },
          {
            in: 'body',
            name: 'user',
            description: 'Updated subcategory object',
            required: true,
            schema: {
              $ref: '#/definitions/Subcategory/update'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      delete: {
        tags: ['Subcategory'],
        summary: 'Delete subcategory by ID',
        description: 'Delete single subcategory data',
        operationId: 'deleteSubcategory',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Delete single subcategory data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/orderHistory': {
      post: {
        tags: ['OrderHistory'],
        summary: 'Add a new orderhistory',
        description: '',
        operationId: 'createOrderHistory',
        parameters: [
          {
            name: 'orderhistory',
            in: 'body',
            schema: {
              $ref: '#/definitions/Orderhistory/create'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      get: {
        tags: ['OrderHistory'],
        summary: 'Find orderhistory',
        description: '',
        operationId: 'getSubcategories',
        parameters: [
          {
            name: 'status',
            in: 'query',
            description: 'Return Based on user status',
            type: 'string',
            enum: ['ACTIVE', 'INACTIVE', 'DELETED']
          },
          {
            name: 'page',
            in: 'query',
            type: 'number',
            default: 1
          },
          {
            name: 'limit',
            in: 'query',
            type: 'number',
            minimum: 1,
            maximum: 100,
            default: 20
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/orderHistory/{id}': {
      get: {
        tags: ['OrderHistory'],
        summary: 'Find orderhistory by ID',
        description: 'Returns a single orderhistory',
        operationId: 'getOrderHistoryById',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Return single orderhistory data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/address': {
      post: {
        tags: ['Address'],
        summary: 'Add a new address',
        description: '',
        operationId: 'createAddress',
        parameters: [
          {
            name: 'address',
            in: 'body',
            schema: {
              $ref: '#/definitions/Address/create'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      get: {
        tags: ['Address'],
        summary: 'Find address',
        description: '',
        operationId: 'getAddress',
        parameters: [
          {
            name: 'status',
            in: 'query',
            description: 'Return Based on user status',
            type: 'string',
            enum: ['ACTIVE', 'INACTIVE', 'DELETED']
          },
          {
            name: 'page',
            in: 'query',
            type: 'number',
            default: 1
          },
          {
            name: 'limit',
            in: 'query',
            type: 'number',
            minimum: 1,
            maximum: 100,
            default: 20
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/address/{id}': {
      get: {
        tags: ['Address'],
        summary: 'Find address by ID',
        description: 'Returns a single address',
        operationId: 'getAddressById',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Return single address data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      put: {
        tags: ['Address'],
        summary: 'Update an existing address',
        description: '',
        operationId: 'updateAddress',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'ID of address that needs to be updated',
            required: true,
            type: 'string'
          },
          {
            in: 'body',
            name: 'user',
            description: 'Updated address object',
            required: true,
            schema: {
              $ref: '#/definitions/Address/update'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      delete: {
        tags: ['Address'],
        summary: 'Delete address by ID',
        description: 'Delete single address data',
        operationId: 'deleteAddress',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Delete single address data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/promocode': {
      post: {
        tags: ['Promocode'],
        summary: 'Add a new promocode',
        description: '',
        operationId: 'createPromocode',
        parameters: [
          {
            name: 'promocode',
            in: 'body',
            schema: {
              $ref: '#/definitions/Promocode/create'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      get: {
        tags: ['Promocode'],
        summary: 'Find promocode',
        description: '',
        operationId: 'getPromocode',
        parameters: [
          {
            name: 'status',
            in: 'query',
            description: 'Return Based on user status',
            type: 'string',
            enum: ['ACTIVE', 'INACTIVE', 'DELETED']
          },
          {
            name: 'valid_from',
            in: 'query',
            description: 'Return Based on starting date',
            type: 'date'
          },
          {
            name: 'valid_to',
            in: 'query',
            description: 'Return Based on ending date',
            type: 'date'
          },
          {
            name: 'code',
            in: 'query',
            description: 'Return Based on code',
            type: 'string'
          },
          {
            name: 'page',
            in: 'query',
            type: 'number',
            default: 1
          },
          {
            name: 'limit',
            in: 'query',
            type: 'number',
            minimum: 1,
            maximum: 100,
            default: 20
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/promocode/{id}': {
      get: {
        tags: ['Promocode'],
        summary: 'Find promocode by ID',
        description: 'Returns a single promocode',
        operationId: 'getPromocodeById',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Return single promocode data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      put: {
        tags: ['Promocode'],
        summary: 'Update an existing promocode',
        description: '',
        operationId: 'updatePromocode',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'ID of promocode that needs to be updated',
            required: true,
            type: 'string'
          },
          {
            in: 'body',
            name: 'user',
            description: 'Updated promocode object',
            required: true,
            schema: {
              $ref: '#/definitions/Promocode/update'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      delete: {
        tags: ['Promocode'],
        summary: 'Delete promocode by ID',
        description: 'Delete single promocode data',
        operationId: 'deletePromocode',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Delete single promocode data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/offers': {
      post: {
        tags: ['Offers'],
        summary: 'Add a new offers',
        description: '',
        operationId: 'createOffers',
        parameters: [
          {
            name: 'offers',
            in: 'body',
            schema: {
              $ref: '#/definitions/Offers/create'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      get: {
        tags: ['Offers'],
        summary: 'Find offers',
        description: '',
        operationId: 'getOffers',
        parameters: [
          {
            name: 'status',
            in: 'query',
            description: 'Return Based on user status',
            type: 'string',
            enum: ['ACTIVE', 'INACTIVE', 'DELETED']
          },
          {
            name: 'valid_from',
            in: 'query',
            description: 'Return Based on starting date',
            type: 'date'
          },
          {
            name: 'valid_to',
            in: 'query',
            description: 'Return Based on ending date',
            type: 'date'
          },
          {
            name: 'code',
            in: 'query',
            description: 'Return Based on code',
            type: 'string'
          },
          {
            name: 'page',
            in: 'query',
            type: 'number',
            default: 1
          },
          {
            name: 'limit',
            in: 'query',
            type: 'number',
            minimum: 1,
            maximum: 100,
            default: 20
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/offers/{id}': {
      get: {
        tags: ['Offers'],
        summary: 'Find offers by ID',
        description: 'Returns a single offers',
        operationId: 'getOffersById',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Return single offers data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      put: {
        tags: ['Offers'],
        summary: 'Update an existing offers',
        description: '',
        operationId: 'updateOffers',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'ID of offers that needs to be updated',
            required: true,
            type: 'string'
          },
          {
            in: 'body',
            name: 'user',
            description: 'Updated offers object',
            required: true,
            schema: {
              $ref: '#/definitions/Offers/update'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      delete: {
        tags: ['Offers'],
        summary: 'Delete offers by ID',
        description: 'Delete single offers data',
        operationId: 'deleteOffers',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Delete single offers data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },

    '/wishlist': {
      post: {
        tags: ['wishlist'],
        summary: 'Add a new wishlist',
        description: '',
        operationId: 'createWishlist',
        parameters: [
          {
            name: 'wishlist',
            in: 'body',
            schema: {
              $ref: '#/definitions/Wishlist/create'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      get: {
        tags: ['wishlist'],
        summary: 'Find wishlist',
        description: '',
        operationId: 'getSubcategories',
        parameters: [
          {
            name: 'status',
            in: 'query',
            description: 'Return Based on user status',
            type: 'string',
            enum: ['ACTIVE', 'INACTIVE', 'DELETED']
          },
          {
            name: 'page',
            in: 'query',
            type: 'number',
            default: 1
          },
          {
            name: 'limit',
            in: 'query',
            type: 'number',
            minimum: 1,
            maximum: 100,
            default: 20
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/wishlist/{id}': {
      get: {
        tags: ['wishlist'],
        summary: 'Find wishlist by ID',
        description: 'Returns a single wishlist',
        operationId: 'getWishlistById',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Return single wishlist data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/brands': {
      post: {
        tags: ['brands'],
        summary: 'Add a new brands',
        description: '',
        operationId: 'createBrands',
        parameters: [
          {
            name: 'brands',
            in: 'body',
            schema: {
              $ref: '#/definitions/Brands/create'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      get: {
        tags: ['brands'],
        summary: 'Find brand',
        description: '',
        operationId: 'getBrands',
        parameters: [
          {
            name: 'status',
            in: 'query',
            description: 'Return Based on user status',
            type: 'string',
            enum: ['ACTIVE', 'INACTIVE', 'DELETED']
          },
          {
            name: 'page',
            in: 'query',
            type: 'number',
            default: 1
          },
          {
            name: 'limit',
            in: 'query',
            type: 'number',
            minimum: 1,
            maximum: 100,
            default: 20
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/brands/{id}': {
      get: {
        tags: ['brands'],
        summary: 'Find brands by ID',
        description: 'Returns a single brands',
        operationId: 'getBrandsById',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Return single brand data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      put: {
        tags: ['brands'],
        summary: 'Update an existing brands',
        description: '',
        operationId: 'updateBrands',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'ID of brand that needs to be updated',
            required: true,
            type: 'string'
          },
          {
            in: 'body',
            name: 'user',
            description: 'Updated brand object',
            required: true,
            schema: {
              $ref: '#/definitions/Brands/update'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      delete: {
        tags: ['brands'],
        summary: 'brands brand by ID',
        description: 'delete single brands data',
        operationId: 'deleteBrands',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Delete single brands data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },

    '/fileuploads/': {
      post: {
        tags: ['Uploads'],
        summary: 'uploads',
        description: 'To upload',
        operationId: 'uploadFile',
        consumes: ['multipart/form-data'],
        produces: ['application/json'],
        parameters: [
          {
            name: 'type',
            in: 'formData',
            description: 'Module type',
            required: true,
            type: 'string',
            enum: ['PROFILE', 'SUBCATEGORY', 'CATEGORY', 'PRODUCT']
          },
          {
            name: 'categoryId',
            in: 'formData',
            description: 'id',
            type: 'string'
          },
          {
            name: 'subcategoryId',
            in: 'formData',
            description: 'id',
            type: 'string'
          },
          {
            name: 'userId',
            in: 'formData',
            description: 'id',
            type: 'string'
          },
          {
            name: 'productId',
            in: 'formData',
            description: 'id',
            type: 'string'
          },
          {
            name: 'file_to_upload',
            in: 'formData',
            description: 'file to upload',
            required: true,
            type: 'file'
          }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/fileuploads/{id}': {
      delete: {
        tags: ['Uploads'],
        summary: 'Deletes a file',
        description: 'To deletes file',
        operationId: 'deletes3',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'KeyID to delete file',
            required: true,
            type: 'string'
          },
          {
            name: 'type',
            in: 'body',
            description: 'Type of file',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/notification': {
      post: {
        tags: ['Notifications'],
        summary: 'Add a new notifications',
        description: '',
        operationId: 'createNotifications',
        parameters: [
          {
            name: 'notifications',
            in: 'body',
            schema: {
              $ref: '#/definitions/Notifications/create'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          201: {
            $ref: '#/definitions/Response/201'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      get: {
        tags: ['Notifications'],
        summary: 'Find notifications',
        description: '',
        operationId: 'getSubcategories',

        parameters: [
          {
            name: 'status',
            in: 'query',
            description: 'Return Based on user status',
            type: 'string',
            enum: ['ACTIVE', 'INACTIVE', 'DELETED']
          },
          {
            name: 'page',
            in: 'query',
            type: 'number',
            default: 1
          },
          {
            name: 'limit',
            in: 'query',
            type: 'number',
            minimum: 1,
            maximum: 100,
            default: 20
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/notification/{id}': {
      get: {
        tags: ['Notifications'],
        summary: 'Find notifications by ID',
        description: 'Returns a single notifications',
        operationId: 'getNotificationsById',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Return single notifications data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      put: {
        tags: ['Notifications'],
        summary: 'Update an existing notifications',
        description: '',
        operationId: 'updateNotifications',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'ID of notifications that needs to be updated',
            required: true,
            type: 'string'
          },
          {
            in: 'body',
            name: 'user',
            description: 'Updated notifications object',
            required: true,
            schema: {
              $ref: '#/definitions/Notifications/update'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      },
      delete: {
        tags: ['Notifications'],
        summary: 'Delete notifications by ID',
        description: 'Delete single notifications data',
        operationId: 'deleteNotifications',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Delete single notifications data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    },
    '/notification/setNotification': {
      put: {
        tags: ['Notifications'],
        summary: 'Update an existing notifications',
        description: '',
        operationId: 'updateNotifications',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'ID of notifications that needs to be updated',
            required: true,
            type: 'string'
          },
          {
            in: 'body',
            name: 'user',
            description: 'Updated notifications object',
            required: true,
            schema: {
              $ref: '#/definitions/Notifications/update'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/200'
          },
          400: {
            $ref: '#/definitions/Response/400'
          },
          401: {
            $ref: '#/definitions/Response/401'
          },
          404: {
            $ref: '#/definitions/Response/404'
          },
          500: {
            $ref: '#/definitions/Response/500'
          }
        }
      }
    }
  },
  definitions
}
