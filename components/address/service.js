const Address = require('./schema')
const { errLogger } = require('../../config/logger')

class Service {
  async createAddress (params) {
    const data = new Address(params)
    return data.save().catch((e) => {
      errLogger.error({ method: 'Address-save', message: e.message })
    })
  }

  async getAddressByUser (user) {
    const data = await Address.find({ userId: user.id }).select('flat_no building_name street city area pincode landmark is_default state').lean()
    return { data: data, count: data ? data.length : 0 }
  }

  async getById (id) {
    return Address.findById(id).lean().catch((e) => {
      errLogger.error({ method: 'Address-getById', message: e.message })
    })
  }

  async updateAddress (id, updateParams) {
    return Address.updateOne(id, updateParams).catch((e) => {
      errLogger.error({ method: 'Address-getById', message: e.message })
    })
  }

  async deleteAddress (id, updateParams) {
    return Address.remove(id, updateParams).catch((e) => {
      errLogger.error({ method: 'Address-getById', message: e.message })
    })
  }
}

const addressService = new Service()

module.exports = { Service, addressService }
