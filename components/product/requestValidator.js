const joi = require('@hapi/joi')

const read = joi.object().keys()

const list = joi.object().keys()

const create = joi.object().keys({
  name: joi.string().required(),
  description: joi.string().required(),
  categoryId: joi.string().required(),
  subcategoryId: joi.string().required(),
  brandId: joi.string().required(),
  image: joi.array().required(),
  price: joi.string().required(),
  price_range: joi.array()
    .items({
      label: joi.string()
        .required(),
      price: joi.string()
        .required(),
      alert_stock: joi.string()
        .required(),
      stock: joi.number()
        .required()
    }),
  rate_per: joi.string().required(),
  total_stock: joi.string().required(),
  alert_stock: joi.string().required(),
  status: joi.string()
})

const update = joi.object().keys({
  name: joi.string(),
  description: joi.string(),
  categoryId: joi.string(),
  subcategoryId: joi.string(),
  brandId: joi.string(),
  image: joi.array(),
  price: joi.string(),
  price_range: joi.array(),
  rate_per: joi.string(),
  total_stock: joi.string(),
  alert_stock: joi.string()
})

const remove = joi.object().keys()

module.exports = {
  list,
  read,
  create,
  update,
  remove
}
