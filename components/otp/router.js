const express = require('express')
const router = express.Router()
const Controller = require('./controller')
const Validator = require('../base/Validator')
const requestValidator = require('./requestValidator')

const controller = new Controller()
const validator = new Validator()

router
  .route('/')
  .get(
    validator.validateRequest.bind(
      new Validator().init(requestValidator.generateOtp)
    ),
    controller.list.bind(controller)
  )

router
  .route('/generate')
  .post(
    validator.validateRequest.bind(
      new Validator().init(requestValidator.generateOtp)
    ),
    controller.generateOtp.bind(controller)
  )

router
  .route('/verifyOtp')
  .post(
    validator.validateRequest.bind(
      new Validator().init(requestValidator.verifyOtp)
    ),
    controller.verifyOtp.bind(controller)
  )
router
  .route('/sendOtp')
  .post(
    validator.validateRequest.bind(
      new Validator().init(requestValidator.sendOtp)
    ),
    controller.sendOtp.bind(controller)
  )

module.exports = router
