const joi = require('@hapi/joi')

const read = joi.object().keys()

const list = joi.object().keys()

const create = joi.object().keys({
  flat_no: joi.string().required(),
  building_name: joi.string().required(),
  first_name: joi.string().required(),
  last_name: joi.string().required(),
  mobile_number: joi.string().required(),
  street: joi.string().required(),
  area: joi.string().required(),
  city: joi.string().required(),
  pincode: joi.number().required(),
  landmark: joi.string().required(),
  is_default: joi.string(),
  state: joi.string().required()
})

const update = joi.object().keys({
  flat_no: joi.string(),
  building_name: joi.string(),
  first_name: joi.string(),
  last_name: joi.string(),
  mobile_number: joi.string(),
  street: joi.string(),
  area: joi.string(),
  city: joi.string(),
  pincode: joi.number().required(),
  landmark: joi.string(),
  is_default: joi.string(),
  state: joi.string()
})

const remove = joi.object().keys()

module.exports = {
  list,
  read,
  create,
  update,
  remove
}
