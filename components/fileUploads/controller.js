const _ = require('lodash')
const { ERROR, SUCCESS } = require('../../libs/constants')
const BaseController = require('../base/controller')
const { uploads } = require('../../config/uploads')
const fse = require('fs-extra')
const { apimsgconfig } = require('../../libs/swagger/api_message')

class Controller extends BaseController {
  static fileSuccessAction (files) {
    const result = []
    _.forEach(files, file => {
      result.push({
        originalname: file.originalname,
        filename: file.filename,
        path: file.path.replace('uploads/', ''),
        size: file.size
      })
    })
    return result
  }

  async putObject (req, res) {
    uploads(req, res, err => {
      const type = req.body.type.toLowerCase()
      if (type === 'category' || type === 'subcategory' || type === 'product' || type === 'profile') {
        let result
        if (req.files && req.files.length) {
          result = Controller.fileSuccessAction(req.files)
        } if (err) {
          this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[551], code: '551' })
        }
        this.sendResponse(req, res, SUCCESS.CODE, { data: result })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[400], code: '400' })
      }
    })
  }

  async deleteObject (req, res) {
    const type = req.body.type.toLowerCase()
    let prefix = `uploads/${type}`
    let id
    if (type === 'category') {
      id = `${req.body.categoryId}`
    } else if (type === 'subcategory') {
      id = `${req.body.subcategoryId}`
    } else if (type === 'profile') {
      id = `${req.body.userId}`
    } else if (type === 'product') {
      id = `${req.body.productId}`
    }
    prefix = `${prefix}/${id}`
    if (prefix && fse.existsSync(prefix)) {
      try {
        await fse.remove(prefix)
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[204], code: '204' })
      } catch (error) {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[599], code: '599' })
      }
    } else {
      this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[400], code: '400' })
    }
  }
}

module.exports = Controller
