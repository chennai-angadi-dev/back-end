const _ = require('lodash')
const BaseController = require('../base/controller')
const { wishlistService: service } = require('./service')
const { SUCCESS, ERROR } = require('../../libs/constants')
const { apimsgconfig } = require('../../libs/swagger/api_message')

class Controller extends BaseController {
  async createWishlist (req, res, next) {
    try {
      const result = await service.addOrRemoveWishlist({ userId: req.user.id, productId: req.body.productId })
      if (result.success === true) {
        if (result.type === 'Add') {
          this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[240], code: '202', data: [{ productId: req.body.productId }] })
        } else {
          this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[241], code: '241', data: [{ productId: req.body.productId }] })
        }
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[599], code: '599' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getWishlists (req, res, next) {
    try {
      const wishlists = await service.getWishlistAndCount(req)
      if (!_.isEmpty(wishlists) && wishlists.count > 0) {
        this.sendResponse(req, res, SUCCESS.CODE, { data: wishlists.data, count: wishlists.count })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getWishlistById (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.getById(id)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[200], code: '200', data: result })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }
}

module.exports = Controller
