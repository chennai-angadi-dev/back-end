var BaseJoi = require('@hapi/joi')
var joidate = require('joi-date-extensions')
const joi = BaseJoi.extend(joidate)

const read = joi.object().keys()

const list = joi.object().keys()

const create = joi.object().keys({
  name: joi.string().required(),
  code: joi.string().required(),
  description: joi.string(),
  type: joi.string().required(),
  discount_value: joi.number().required(),
  valid_from: joi.date().format('YYYY-MM-DD').iso().required(),
  valid_to: joi.date().format('YYYY-MM-DD').iso().greater(joi.ref('valid_from')).required(),
  minimum_order_amount: joi.number().required(),
  maximum_number_of_users: joi.number().required(),
  image: joi.string().required(),
  status: joi.string()
})

const update = joi.object().keys()

const remove = joi.object().keys()

module.exports = {
  list,
  read,
  create,
  update,
  remove
}
