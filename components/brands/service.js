const Brand = require('./schema')
const { errLogger } = require('../../config/logger')

class Service {
  async createBrand (params) {
    const data = new Brand(params)
    return data.save().catch((e) => {
      errLogger.error({ method: 'Brand-save', message: e.message })
    })
  }

  getBrandAndCount (req) {
    const reqQuery = req.query
    const limit = Number(
      reqQuery && reqQuery.limit ? reqQuery.limit : process.env.PAGE_LIMIT
    )
    const pageNo = Number(reqQuery && reqQuery.page ? limit * reqQuery.page - limit : 0)
    const sortField = reqQuery && reqQuery.sort ? reqQuery.sort : 'createdAt'
    const order = reqQuery && reqQuery.order && reqQuery.order === 'asc' ? 1 : -1
    const match = { $and: [] }

    if (reqQuery.status) {
      match.$and.push({ status: reqQuery.status })
    } else {
      match.$and.push({ status: 'ACTIVE' })
    }

    if (reqQuery.name) {
      match.$and.push({ name: { $regex: reqQuery.name, $options: 'i' } })
    }

    if (reqQuery.category) {
      match.$and.push({ 'categoryId.name': { $regex: reqQuery.category, $options: 'i' } })
    }

    return Brand.aggregate([
      {
        $lookup: {
          from: 'categories',
          let: { id: '$categoryId' },
          pipeline: [
            { $match: { $expr: { $eq: ['$_id', '$$id'] } } },
            { $project: { name: 1 } }
          ],
          as: 'categoryId'
        }
      },
      {
        $unwind: { path: '$categoryId', preserveNullAndEmptyArrays: true }
      },
      { $project: { name: 1, categoryId: 1, status: 1 } },
      {
        $match: match
      },
      {
        $facet: {
          results: [
            { $sort: { [`${sortField}`]: order } },
            { $skip: pageNo },
            { $limit: limit }
          ],
          totalCount: [
            {
              $count: 'count'
            }
          ]
        }
      }
    ])
  }

  async getAll () {
    return Brand.find()
      .populate({
        path: 'categoryId',
        select: 'name'
      })
      .select('name status categoryId')
      .sort({ createdAt: -1 }).lean().catch((e) => {
        errLogger.error({ method: 'Brand-getAll', message: e.message })
      })
  }

  async getById (id) {
    return Brand.findById(id)
      .populate({
        path: 'categoryId',
        select: 'name'
      })
      .select('name status categoryId')
      .lean().catch((e) => {
        errLogger.error({ method: 'Brand-getById', message: e.message })
      })
  }

  async updateBrand (id, updateParams) {
    return Brand.updateOne(id, updateParams).catch((e) => {
      errLogger.error({ method: 'Brand-getById', message: e.message })
    })
  }

  async deleteBrand (id, updateParams) {
    return Brand.updateOne(id, updateParams).catch((e) => {
      errLogger.error({ method: 'Brand-getById', message: e.message })
    })
  }
}

const BrandService = new Service()

module.exports = { Service, BrandService }
