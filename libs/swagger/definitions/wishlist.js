module.exports = {
  create: {
    required: [
      'productId'
    ],
    properties: {
      productId: {
        type: 'string'
      }
    }
  }
}
