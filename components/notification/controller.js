const _ = require('lodash')
const BaseController = require('../base/controller')
const { notificationService: service } = require('./service')
const { SUCCESS, ERROR } = require('../../libs/constants')
const { apimsgconfig } = require('../../libs/swagger/api_message')

class Controller extends BaseController {
  async createNotification (req, res, next) {
    try {
      const data = await service.createNotification(req)
      if (!_.isEmpty(data)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: 'Notification created successfully' })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: 'server_error.try_again' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getNotifications (req, res, next) {
    try {
      const notifications = await service.getNotificationAndCount(req)
      if (!_.isEmpty(notifications) && notifications.count > 0) {
        this.sendResponse(req, res, SUCCESS.CODE, { data: notifications.data, count: notifications.count })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getNotificationById (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.getById(id)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { data: result })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: 'No records found' })
      }
    } catch (e) {
      next(e)
    }
  }

  async updateNotification (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.updateNotification({ _id: id }, req.body)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: 'Updated notification details successfully' })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: 'No records found' })
      }
    } catch (e) {
      next(e)
    }
  }

  async enableNotification (req, res, next) {
    try {
      const result = await service.enableNotification({ _id: req.user.id }, req.body)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: 'Updated notification details successfully' })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: 'No records found' })
      }
    } catch (e) {
      next(e)
    }
  }

  async deleteNotification (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.deleteNotification({ _id: id })
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: 'Deleted notification details successfully' })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: 'No records found' })
      }
    } catch (e) {
      next(e)
    }
  }
}

module.exports = Controller
