module.exports = {
  required: [
    'name',
    'categoryId',
    'image'
  ],
  create: {
    required: [
      'name',
      'categoryId',
      'image'
    ],
    properties: {
      name: {
        type: 'string',
        index: true,
        trim: true,
        unique: true,
        required: [true, 'Name is required.']
      },
      status: {
        type: 'string',
        default: 'ACTIVE',
        enum: ['ACTIVE', 'INACTIVE', 'DELETED ']
      },
      image: {
        type: 'string',
        trim: true
      }
    }
  },
  update: {
    properties: {
      name: {
        type: 'string',
        index: true,
        trim: true,
        unique: true,
        required: [true, 'Name is required.']
      },
      status: {
        type: 'string',
        default: 'ACTIVE',
        enum: ['ACTIVE', 'INACTIVE', 'DELETED ']
      },
      image: {
        type: 'string',
        trim: true
      }
    }
  },
  properties: {
    name: {
      type: 'string',
      index: true,
      trim: true,
      unique: true,
      required: [true, 'Name is required.']
    },
    status: {
      type: 'string',
      default: 'ACTIVE',
      enum: ['ACTIVE', 'INACTIVE', 'DELETED ']
    },
    image: {
      type: 'string',
      trim: true
    }
  }
}
