const Promocodehistory = require('./schema')
const { errLogger } = require('../../config/logger')
const Promocode = require('../promocode/schema')
const Orders = require('../orders/schema')

class Service {
  async createPromocode (req) {
    var promocodehistory = await Promocodehistory.findOne({ user_id: req.user.id, orderId: req.body.orderId, status: 'APPLIED' })
    if (promocodehistory) {
      return { data: {}, status: 'promocode already applied' }
    }
    var validPromoCode = await Promocode.findOne({ code: req.body.code, status: 'ACTIVE', valid_from: { $lte: new Date() }, valid_to: { $gte: new Date() } }).lean()
    if (!validPromoCode) {
      return { data: {}, status: 'invalid promocode expired' }
    }
    var totalcount = await Promocodehistory.countDocuments({ promo_code_id: validPromoCode._id, status: 'REDEEM' })
    if (validPromoCode.maximum_number_of_users <= totalcount) {
      return { data: {}, status: 'invalid promocode' }
    }
    var currentOrder = (await Orders.findOne({ _id: req.body.orderId }).lean())
    if (currentOrder.promocodeId) {
      return { data: {}, status: 'promocode already applied' }
    }
    if (currentOrder.order_price < validPromoCode.minimum_order_amount) {
      return { data: {}, status: 'order amount lower than required amount' }
    }
    await Orders.updateOne({ _id: req.body.orderId }, { promocodeId: validPromoCode._id })
    const data = new Promocodehistory({ promo_code_id: validPromoCode._id, orderId: currentOrder._id, user_id: req.user.id, status: 'APPLIED' })
    const createdPromoHistory = (data.save().catch((e) => {
      errLogger.error({ method: 'promocode-save', message: e.message })
    }))
    if (!createdPromoHistory) {
      return { data: {}, status: 'order history not updated' }
    }
    const orderData = await Orders.findOne({ _id: req.body.orderId }).lean()
    return { data: orderData, status: 'success' }
  }

  async getAll () {
    return Promocodehistory.find()
      .populate({
        path: 'orderId'
      })
      .sort({ createdAt: -1 }).lean().catch((e) => {
        errLogger.error({ method: 'Subcategory-getAll', message: e.message })
      })
  }

  async removePromocode (id, updateParams) {
    return Promocodehistory.updateOne(id, updateParams).catch((e) => {
      errLogger.error({ method: 'promocode-getById', message: e.message })
    })
  }
}

const promocodeService = new Service()

module.exports = { Service, promocodeService }
