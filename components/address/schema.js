const mongoose = require('mongoose')
const Schema = mongoose.Schema
const schema = new mongoose.Schema(
  {
    flat_no: {
      type: String,
      trim: true
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'users'
    },
    building_name: {
      type: String,
      trim: true
    },
    first_name: {
      type: String,
      trim: true
    },
    last_name: {
      type: String,
      trim: true
    },
    mobile_number: {
      type: String,
      trim: true
    },
    street: {
      type: String,
      trim: true
    },
    area: {
      type: String,
      trim: true
    },
    city: {
      type: String,
      trim: true
    },
    pincode: {
      type: Number,
      trim: true
    },
    landmark: {
      type: String,
      trim: true
    },
    state: {
      type: String,
      trim: true
    },
    is_default: {
      type: String,
      default: '0',
      enum: ['0', '1']
    }
  },
  { timestamps: true, versionKey: false }
)

const Addresses = mongoose.model('addresses', schema)
module.exports = Addresses
