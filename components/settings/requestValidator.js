const joi = require('@hapi/joi')

const list = joi.object().keys()

const update = joi.object().keys({
  terms: joi.string(),
  privacy: joi.string(),
  about_us: joi.string(),
  image: joi.string(),
  email_us: joi.string()
})

const create = joi.object().keys({
  terms: joi.string().required(),
  privacy: joi.string().required(),
  about_us: joi.string().required(),
  image: joi.string(),
  email_us: joi.string()
})

module.exports = {
  list,
  create,
  update
}
