const mongoose = require('mongoose')
const Schema = mongoose.Schema

const schema = new mongoose.Schema(
  {
    name: {
      type: String,
      index: true,
      trim: true,
      unique: true,
      required: [true, 'Name is required.']
    },
    categoryId: {
      type: Schema.Types.ObjectId,
      ref: 'categories',
      required: [true, 'category is required.']
    },
    status: {
      type: String,
      default: 'ACTIVE',
      enum: ['ACTIVE', 'ACTIVE', 'DELETED ']
    },
    image: {
      type: String,
      trim: true
    }
  },
  { timestamps: true, versionKey: false }
)

const Subcategories = mongoose.model('subcategories', schema)
module.exports = Subcategories
