var BaseJoi = require('@hapi/joi')
var joidate = require('joi-date-extensions')
const joi = BaseJoi.extend(joidate)

const read = joi.object().keys()

const list = joi.object().keys()

const create = joi.object().keys({
  name: joi.string().required(),
  categoryId: joi.string().required(),
  productId: joi.string().required(),
  description: joi.string(),
  type: joi.string().required(),
  discount_value: joi.number().required(),
  valid_from: joi.date().format('YYYY-MM-DD').iso().required(),
  valid_to: joi.date().format('YYYY-MM-DD').iso().greater(joi.ref('valid_from')).required(),
  minimum_order_amount: joi.number().required(),
  maximum_discount_amount: joi.number().required(),
  image: joi.string().required(),
  status: joi.string()
})

const update = joi.object().keys({
  name: joi.string(),
  categoryId: joi.string(),
  productId: joi.string(),
  description: joi.string(),
  type: joi.string(),
  discount_value: joi.number(),
  valid_from: joi.date().format('YYYY-MM-DD').iso(),
  valid_to: joi.date().format('YYYY-MM-DD').iso().greater(joi.ref('valid_from')),
  maximum_discount_amount: joi.number(),
  image: joi.string(),
  status: joi.string()
})

const remove = joi.object().keys()

module.exports = {
  list,
  read,
  create,
  update,
  remove
}
