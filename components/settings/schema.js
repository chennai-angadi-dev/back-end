const mongoose = require('mongoose')
const Schema = mongoose.Schema
const schema = new mongoose.Schema(
  {
    terms: {
      type: String,
      trim: true
    },
    privacy: {
      type: String,
      trim: true
    },
    about_us: {
      type: String,
      trim: true
    },
    image: {
      type: String,
      trim: true
    },
    email_us: {
      type: String,
      trim: true
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'users'
    }
  },
  { timestamps: true, versionKey: false }
)

const Setting = mongoose.model('settings', schema)
module.exports = Setting
