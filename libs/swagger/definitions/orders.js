module.exports = {
  create: {
    required: [
      'delivery_charge',
      'package_charge',
      'tax_amount',
      'promocodeId',
      'total_price',
      'order_price',
      'total_products',
      'addressId',
      'product'
    ],
    properties: {
      delivery_charge: {
        type: 'number'
      },
      package_charge: {
        type: 'number'
      },
      tax_amount: {
        type: 'number'
      },
      promocodeId: {
        type: 'string'
      },
      total_price: {
        type: 'number'
      },
      order_price: {
        type: 'number'
      },
      total_products: {
        type: 'number'
      },
      addressId: {
        type: 'string'
      },
      product: {
        type: 'array',
        items: {
          type: 'string'
        }
      }
    }
  },
  update: {
    properties: {
      delivery_charge: {
        type: 'number'
      },
      package_charge: {
        type: 'number'
      },
      tax_amount: {
        type: 'number'
      },
      promocodeId: {
        type: 'string'
      },
      total_price: {
        type: 'number'
      },
      order_price: {
        type: 'number'
      },
      total_products: {
        type: 'number'
      },
      addressId: {
        type: 'string'
      },
      product: {
        type: 'array',
        items: {
          type: 'string'
        }
      }
    }
  },
  updatecart: {
    required: [
      'productId',
      'product_count'
    ],
    properties: {
      productId: {
        type: 'string'
      },
      product_count: {
        type: 'string'
      }
    }
  },

  delete: {
    required: [
      'productId'
    ],
    properties: {
      productId: {
        type: 'string'
      }
    }
  },

  properties: {
    properties: {
      delivery_charge: {
        type: 'number'
      },
      package_charge: {
        type: 'number'
      },
      tax_amount: {
        type: 'number'
      },
      promocodeId: {
        type: 'string'
      },
      total_price: {
        type: 'number'
      },
      order_price: {
        type: 'number'
      },
      total_products: {
        type: 'number'
      },
      addressId: {
        type: 'string'
      },
      product: {
        type: 'array',
        items: {
          type: 'string'
        }
      }
    }
  }
}
