module.exports = {
  create: {
    required: [
      'name',
      'categoryId',
      'image'
    ],
    properties: {
      name: {
        type: 'string'
      },
      image: {
        type: 'string'
      },
      categoryId: {
        type: 'string'
      },
      status: {
        type: 'string',
        default: 'ACTIVE',
        enum: ['ACTIVE', 'INACTIVE', 'DELETED']
      }
    }
  },
  update: {
    properties: {
      name: {
        type: 'string'
      },
      image: {
        type: 'string'
      },
      categoryId: {
        type: 'string'
      },
      status: {
        type: 'string',
        default: 'ACTIVE',
        enum: ['ACTIVE', 'INACTIVE', 'DELETED']
      }
    }
  },
  properties: {
    name: {
      type: 'string'
    },
    image: {
      type: 'string'
    },
    categoryId: {
      type: 'string'
    },
    status: {
      type: 'string',
      default: 'ACTIVE',
      enum: ['ACTIVE', 'INACTIVE', 'DELETED']
    }
  }
}
