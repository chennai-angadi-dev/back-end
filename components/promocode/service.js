const Promocode = require('./schema')
const { errLogger } = require('../../config/logger')

class Service {
  async createPromocode (params) {
    var count = await Promocode.count({ code: params.code, status: { $ne: 'DELETED' } })
    if (count > 0) {
      return 400
    }
    const data = new Promocode(params)
    return (data.save().catch((e) => {
      errLogger.error({ method: 'promocode-save', message: e.message })
    }) ? 202 : 599)
  }

  async getPromocodeAndCount (req) {
    var reqQuery = req.query
    const limit = Number(
      reqQuery && reqQuery.limit ? reqQuery.limit : process.env.PAGE_LIMIT
    )
    const pageNo = Number(reqQuery && reqQuery.page ? limit * reqQuery.page - limit : 0)
    const sortField = reqQuery && reqQuery.sort ? reqQuery.sort : 'createdAt'
    const order = reqQuery && reqQuery.order && reqQuery.order === 'asc' ? 1 : -1
    const match = { $and: [] }

    if (reqQuery.status) {
      match.$and.push({ status: reqQuery.status })
    } else {
      match.$and.push({ status: 'ACTIVE' })
    }

    if (reqQuery.code) {
      match.$and.push({ code: { $regex: reqQuery.code, $options: 'i' } })
    }

    if (reqQuery.valid_from) {
      match.$and.push({ valid_from: { $lte: new Date(reqQuery.valid_from) } })
    }
    if (reqQuery.valid_to) {
      match.$and.push({ valid_to: { $gte: new Date(reqQuery.valid_to) } })
    }
    return Promocode.aggregate([
      {
        $match: match
      },
      { $project: { name: 1, code: 1, description: 1, type: 1, discount_value: 1, valid_from: 1, valid_to: 1, maximum_number_of_users: 1, minimum_order_amount: 1, image: 1, status: 1 } },
      {
        $facet: {
          results: [
            { $sort: { [`${sortField}`]: order } },
            { $skip: pageNo },
            { $limit: limit }
          ],
          totalCount: [
            {
              $count: 'count'
            }
          ]
        }
      }
    ])
  }

  async getById (id) {
    return Promocode.findById(id).lean().catch((e) => {
      errLogger.error({ method: 'promocode-getById', message: e.message })
    })
  }

  async updatePromocode (id, updateParams) {
    return Promocode.updateOne(id, updateParams).catch((e) => {
      errLogger.error({ method: 'promocode-getById', message: e.message })
    })
  }

  async deletePromocode (id, updateParams) {
    return Promocode.updateOne(id, updateParams).catch((e) => {
      errLogger.error({ method: 'promocode-getById', message: e.message })
    })
  }
}

const promocodeService = new Service()

module.exports = { Service, promocodeService }
