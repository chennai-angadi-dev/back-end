module.exports = {
  required: [
    'notify_types',
    'notify_offer',
    'notify_promocode',
    'userId',
    'number_of_orders',
    'schedule_date',
    'sent_date',
    'status',
    'message'
  ],
  create: {
    required: [
      'notify_types',
      'notify_offer',
      'notify_promocode',
      'userId',
      'number_of_orders',
      'schedule_date',
      'sent_date',
      'status',
      'message'
    ],
    properties: {
      notify_types: {
        type: 'string'
      },
      notify_offer: {
        type: 'string'
      },
      notify_promocode: {
        type: 'string'
      },
      message: {
        type: 'string'
      },
      userId: {
        type: 'string'
      },
      number_of_orders: {
        type: 'number'
      },
      schedule_date: {
        type: 'string',
        format: 'date'
      },
      sent_date: {
        type: 'string',
        format: 'date'
      },
      status: {
        type: String,
        default: 'ACTIVE',
        enum: ['ACTIVE', 'INACTIVE']
      }
    }
  },
  update: {
    properties: {
      notify_types: {
        type: 'string'
      },
      notify_offer: {
        type: 'string'
      },
      message: {
        type: 'string'
      },
      notify_promocode: {
        type: 'string'
      },
      userId: {
        type: 'string'
      },
      number_of_orders: {
        type: 'number'
      },
      schedule_date: {
        type: 'string',
        format: 'date'
      },
      sent_date: {
        type: 'string',
        format: 'date'
      },
      status: {
        type: String,
        default: 'ACTIVE',
        enum: ['ACTIVE', 'INACTIVE']
      }
    }
  },
  enable: {
    required: [
      'enable_notification'
    ],
    properties: {
      enable_notification: {
        type: 'string'
      }
    }
  },
  properties: {
    notify_types: {
      type: 'string'
    },
    notify_offer: {
      type: 'string'
    },
    notify_promocode: {
      type: 'string'
    },
    userId: {
      type: 'string'
    },
    number_of_orders: {
      type: 'number'
    },
    schedule_date: {
      type: 'string',
      format: 'date'
    },
    sent_date: {
      type: 'string',
      format: 'date'
    },
    status: {
      type: 'string',
      default: 'ACTIVE',
      enum: ['ACTIVE', 'INACTIVE']
    }
  }
}
