const mongoose = require('mongoose')
const Schema = mongoose.Schema
const schema = new mongoose.Schema(
  {
    promo_code_id: {
      type: Schema.Types.ObjectId,
      ref: 'promocodes',
      required: [true, 'promocode is required.']
    },
    orderId: {
      type: Schema.Types.ObjectId,
      ref: 'orders',
      required: [true, 'orders is required.']
    },
    user_id: {
      type: Schema.Types.ObjectId,
      ref: 'users'
    },
    status: {
      type: String,
      default: 'APPLIED',
      enum: ['APPLIED', 'REDEEM']
    }
  },
  { timestamps: true, versionKey: false }
)

const Promocodehistory = mongoose.model('promocodehistory', schema)
module.exports = Promocodehistory
