const mongoose = require('mongoose')
const Schema = mongoose.Schema

const schema = new mongoose.Schema(
  {
    productId: {
      type: Schema.Types.ObjectId,
      ref: 'products',
      required: [true, 'product is required.']
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'users',
      required: [true, 'user is required.']
    }
  },
  { timestamps: true, versionKey: false }
)

const Wishlists = mongoose.model('wishlists', schema)
module.exports = Wishlists
