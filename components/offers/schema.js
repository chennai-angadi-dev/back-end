const mongoose = require('mongoose')
const Schema = mongoose.Schema

const schema = new mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
      required: [true, 'Name is required.']
    },
    categoryId: {
      type: Schema.Types.ObjectId,
      ref: 'categories',
      required: [true, 'category is required.']
    },
    productId: {
      type: Schema.Types.ObjectId,
      ref: 'products',
      required: [true, 'products is required.']
    },
    description: {
      type: String,
      trim: true
    },
    type: {
      type: String,
      enum: ['Flat', '%']
    },
    discount_value: {
      type: Number,
      trim: true,
      required: true
    },
    valid_from: {
      type: Date,
      trim: true,
      required: true
    },
    valid_to: {
      type: Date,
      trim: true,
      required: true
    },
    minimum_order_amount: {
      type: Number,
      trim: true,
      required: true
    },
    maximum_discount_amount: {
      type: Number,
      trim: true,
      required: true
    },
    image: {
      type: String,
      trim: true,
      required: true
    },
    status: {
      type: String,
      default: 'ACTIVE',
      enum: ['ACTIVE', 'INACTIVE', 'DELETED ']
    }
  },
  { timestamps: true, versionKey: false }
)

const Offers = mongoose.model('offers', schema)
module.exports = Offers
