const _ = require('lodash')
const BaseController = require('../base/controller')
const { productService: service } = require('./service')
const { SUCCESS, ERROR } = require('../../libs/constants')
const { apimsgconfig } = require('../../libs/swagger/api_message')

class Controller extends BaseController {
  async createProduct (req, res, next) {
    try {
      const data = await service.createProduct(req.body)
      if (!_.isEmpty(data) && data._id) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[202], code: '202', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[599], code: '599' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getAllProducts (req, res, next) {
    try {
      if (this.isThatValidForFilterQuery(req.query)) {
        const products = await service.filterData(req)
        if (!_.isEmpty(products)) {
          this.sendResponse(req, res, SUCCESS.CODE, {
            message: apimsgconfig.en_us[200],
            code: '200',
            data: products || [],
            count: products ? products.length : 0
          })
          return
        }
      } else {
        const products = await service.getProductAndCount(req)
        if (!_.isEmpty(products)) {
          this.sendResponse(req, res, SUCCESS.CODE, {
            data: products && products[0].results ? products[0].results : [],
            count: products && products[0].totalCount[0] && products[0].totalCount[0].count
              ? products[0].totalCount[0].count
              : 0
          })
          return
        }
      }
      this.sendResponse(req, res, ERROR.CODE, { message: 'No records found' })
    } catch (e) {
      next(e)
    }
  }

  isThatValidForFilterQuery (query) {
    if (!_.isEmpty(query)) {
      if (query.brands || query.category || query.price || query.offer) {
        return true
      }
    }
    return false
  }

  async getProductById (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.getById(id)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[200], code: '200', data: result })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async updateProduct (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.updateProduct({ _id: id, status: 'ACTIVE' }, req.body)
      if (!_.isEmpty(result) && result.nModified !== 0) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[203], code: '203', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[599], code: '599' })
      }
    } catch (e) {
      next(e)
    }
  }

  async deleteProduct (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.deleteProduct({ _id: id }, { status: 'DELETED' })
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[204], code: '204', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }
}

module.exports = Controller
