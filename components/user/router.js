const express = require('express')
const router = express.Router()
const Controller = require('./controller')
const Validator = require('../base/Validator')
const requestValidator = require('./requestValidator')
const { auth, isValidUser } = require('../../helpers/utils')

const controller = new Controller()
const validator = new Validator()

router
  .route('/')
  .post(
    auth,
    isValidUser,
    validator.validateRequest.bind(
      new Validator().init(requestValidator.create)
    ),
    controller.createUser.bind(controller)
  )
router
  .route('/profile')
  .get(
    auth,
    isValidUser,
    validator.validateRequest.bind(
      new Validator().init(requestValidator.read)
    ),
    controller.getProfile.bind(controller)
  )

router
  .route('/profile')
  .put(
    auth,
    isValidUser,
    validator.validateRequest.bind(
      new Validator().init(requestValidator.update)
    ),
    controller.updateProfile.bind(controller)
  )

router
  .route('/logout')
  .get(
    auth,
    isValidUser,
    validator.validateRequest.bind(
      new Validator().init(requestValidator.logout)
    ),
    controller.userLogOut.bind(controller)
  )

router
  .route('/')
  .get(
    auth,
    isValidUser,
    validator.validateRequest.bind(
      new Validator().init(requestValidator.list)
    ),
    controller.getUsers.bind(controller)
  )

router
  .route('/:id')
  .get(
    auth,
    isValidUser,
    validator.validateRequest.bind(
      new Validator().init(requestValidator.read)
    ),
    controller.getUserById.bind(controller)
  )

router
  .route('/:id')
  .put(
    auth,
    validator.validateRequest.bind(
      new Validator().init(requestValidator.update)
    ),
    controller.updateUser.bind(controller)
  )

router
  .route('/:id')
  .delete(
    auth,
    validator.validateRequest.bind(
      new Validator().init(requestValidator.remove)
    ),
    controller.deleteUser.bind(controller)
  )
router
  .route('/signUp')
  .post(
    validator.validateRequest.bind(
      new Validator().init(requestValidator.signUp)
    ),
    controller.signUp.bind(controller)
  )

module.exports = router
