const User = require('./schema')
const { errLogger } = require('../../config/logger')
const ClientError = require('../../helpers/ClientError')
const { STATUS } = require('../../libs/constants')
const _ = require('lodash')
const Otp = require('../otp/schema')

class Service {
  async isValidMobileNumber (query) {
    return User.where({ mobileNumber: query.mobileNumber, status: { $eq: STATUS.ACTIVE } }).countDocuments().catch((e) => {
      errLogger.error({ method: 'users-isMobileNoInactiveOrDeleted', message: e.message })
    })
  }

  async createUser (params) {
    const data = new User(params)
    return data.save().catch((e) => {
      errLogger.error({ method: 'User-save', message: e.message })
    })
  }

  async getUserAndCount (req) {
    const reqQuery = req.query
    const limit = Number(
      reqQuery && reqQuery.limit ? reqQuery.limit : process.env.PAGE_LIMIT
    )
    const pageNo = Number(reqQuery && reqQuery.page ? limit * reqQuery.page - limit : 0)
    const sortField = reqQuery && reqQuery.sort ? reqQuery.sort : 'createdAt'
    const users = reqQuery && reqQuery.users && reqQuery.users === 'asc' ? 1 : -1

    const data = await User.find()
      .populate({
        path: 'addressId',
        options: {
          sort: { [`${sortField}`]: users },
          skip: pageNo,
          limit: limit
        }
      })
      .lean().catch((e) => {
        errLogger.error({ method: 'Order-getAll', message: e.message })
      })
    return { data: data, count: data ? data.length : 0 }
  }

  async getAll () {
    return User.find().sort({ createdAt: -1 }).lean().catch((e) => {
      errLogger.error({ method: 'User-getAll', message: e.message })
    })
  }

  async getById (id) {
    return User.findById(id)
      .populate({
        path: 'addressId',
        select: 'flat_no pincode city landmark area is_default street'
      })
      .lean().catch((e) => {
        errLogger.error({ method: 'User-getById', message: e.message })
      })
  }

  async updateUser (id, updateParams) {
    return User.updateOne(id, updateParams).catch((e) => {
      errLogger.error({ method: 'User-getById', message: e.message })
    })
  }

  async deleteUser (id, updateParams) {
    return User.updateOne(id, updateParams).catch((e) => {
      errLogger.error({ method: 'User-getById', message: e.message })
    })
  }

  async verifyUser (query) {
    const isSocialLogin = query.isSocialLogin
    var user = {}
    if (isSocialLogin === true) {
      if (query.socialid) {
        user = await User.findOne({ socialid: query.socialid }).select('mobile_number status role username is_verified').lean()
      }
    } else {
      user = await User.findOne({ mobile_number: query.mobile_number }).select('mobile_number status role username is_verified').lean()
    }
    if (!user) throw new ClientError('Invalid user')

    if (user.is_verified === '0') { throw new ClientError('User is not verified') }

    if (user.status !== STATUS.ACTIVE) { throw new ClientError('User is not active') }

    return user
  }

  async verifysocialid (req) {
    const user = await User.findOne({ socialid: req.body.socialid }).select('mobile_number socialid').lean()
    return (!_.isNull(user))
  }

  async getUserDetailByMobileNumber (query) {
    return User.findOne(query)
      .lean().catch((e) => {
        errLogger.error({ method: 'users-getUserDetailByMobileNumber', message: e.message })
      })
  }

  async save (params) {
    const data = new Otp(params)
    return data.save().catch((e) => {
      errLogger.error({ method: 'otp-save', message: e.message })
    })
  }
}
const userService = new Service()

module.exports = { Service, userService }
