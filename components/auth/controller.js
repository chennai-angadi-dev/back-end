const _ = require('lodash')
const jwt = require('jsonwebtoken')
const { SUCCESS, ERROR, STATUS } = require('../../libs/constants')
const BaseController = require('../base/controller')
const { userService: service } = require('../user/service')
const { apimsgconfig } = require('../../libs/swagger/api_message')
const utils = require('../../helpers/utils')
const userController = require('../user/controller')

class Controller extends BaseController {
  async sendOtp (req, type) {
    const rand = utils.getRandomNumber(6)
    const otpData = {
      code: rand,
      mobile_number: req.body.mobile_number,
      type: type,
      status: STATUS.UN_VERIFIED
    }
    return await service.save(otpData)
  }

  async authenticateSuccessAction (req, res, resData) {
    const payload = userController.getPayload(resData)
    const token = userController.generateToken(payload)
    if (!_.isEmpty(token)) {
      const decoded = jwt.decode(token)
      var apiMsgIdx
      var apiResponse
      if (req.url === '/verifyOtp') {
        if (req.body.type === 'SIGN_UP') {
          apiMsgIdx = 212
          apiResponse = '212'
          const updatedParams = { is_verified: '1' }
          await service.updateUser({ _id: payload.id }, updatedParams)
        } else {
          const updatedParams = { device_token: req.body.device_token }
          await service.updateUser({ _id: payload.id }, updatedParams)
          apiMsgIdx = 213
          apiResponse = '213'
        }
      } else {
        if (req.body.isSocialLogin === false || _.isNull(req.body.isSocialLogin) || _.isUndefined(req.body.isSocialLogin)) {
          const sendOtp = await this.sendOtp(req, 'LOGIN')
          if (sendOtp) {
            this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[220], code: '220', data: [] })
            return
          } else {
            this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[400], code: '400' })
            return
          }
        } else {
          const updatedParams = { device_token: req.body.device_token }
          await service.updateUser({ _id: payload.id }, updatedParams)
          apiMsgIdx = 213
          apiResponse = '213'
        }
      }

      this.sendResponse(req, res, SUCCESS.CODE, {
        code: apiResponse,
        data: {
          token,
          expired_time: decoded.exp
        },
        message: apimsgconfig.en_us[apiMsgIdx]
      })
    } else {
      this.sendResponse(req, res, ERROR.CODE, {
        message: apimsgconfig.en_us[401], code: '401'
      })
    }
  }

  async verifyUser (req, res, next) {
    try {
      if (req.body.isSocialLogin === true) {
        if (!req.body.socialid) {
          this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[416], code: '416' })
        } else {
          const socialIdExist = await service.verifysocialid(req)
          if (!socialIdExist) {
            this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[400], code: '400' })
          }
        }
      } else {
        const mobileNumber = req.body.mobile_number
        const mob = /^[1-9]{1}[0-9]{9}$/
        if (!mob.test(mobileNumber)) {
          this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[412], code: '412' })
          return false
        }
      }
      const user = await service.verifyUser(req.body)
      if (!_.isEmpty(user) && user._id) {
        this.authenticateSuccessAction(req, res, user)
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[400], code: '400' })
      }
    } catch (e) {
      next(e)
    }
  }
}

module.exports = Controller
