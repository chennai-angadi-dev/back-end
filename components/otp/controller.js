const _ = require('lodash')
const moment = require('moment')
const BaseController = require('../base/controller')
const { otpService: service } = require('./service')
const AuthController = require('../auth/controller')
const { userService: userServ } = require('../user/service')
const utils = require('../../helpers/utils')
const { apimsgconfig } = require('../../libs/swagger/api_message')
const {
  SUCCESS,
  ERROR,
  STATUS,
  OTP_TYPE
} = require('../../libs/constants')

class Controller extends BaseController {
  async generateOtp (req, res) {
    const isInactiveMobile = await userServ.isValidMobileNumber(req.body)
    if (isInactiveMobile >= 1) {
      const userData = await userServ.getUserDetailByMobileNumber({ mobile_number: req.body.mobile_number })
      if (!_.isEmpty(userData) && !_.isEmpty(userData.hospitalId) && userData.hospitalId.paymentStatus === 'UNPAID') {
        this.sendResponse(req, res, ERROR.CODE, { message: 'Your subscription has expired. Please reactivate your account' })
      } else {
        const sendOtp = await this.generateValidOtp(req, res)
        if (sendOtp) {
          this.sendResponse(req, res, SUCCESS.CODE, { message: 'server_success.otp.sent' })
        } else {
          this.sendResponse(req, res, ERROR.CODE, { message: 'server_error.otp.failed' })
        }
      }
    } else {
      this.sendResponse(req, res, ERROR.CODE, { message: 'server_error.invalid_mobile_number' })
    }
  }

  async generateValidOtp (req, res) {
    const mobileNumber = req.body.mobile_number
    const type = req.body.type
    const query = {
      mobileNumber: mobileNumber,
      type: type,
      status: STATUS.UN_VERIFIED
    }
    const optExists = await service.isOtpExist(query)
    if (!_.isUndefined(optExists) && _.isObject(optExists) && optExists.id) {
      if (Controller.checkOtpExpiryTime(optExists)) {
        // send sms code will comes here
        return true
      }
      return this.sendOtp(req, res)
    } else {
      return this.sendOtp(req, res)
    }
  }

  async sendOtp (req, res, rand = null) {
    const mobileNumber = req.body.mobile_number
    const mob = /^[1-9]{1}[0-9]{9}$/
    if (!mob.test(mobileNumber)) {
      this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[412], code: '412' })
      return false
    }
    var otpData = { mobile_number: req.body.mobile_number, type: req.body.type }
    const optExists = await service.isOtpExist(otpData)
    if (!_.isUndefined(optExists) && !_.isNull(optExists) && optExists.id) {
      if (Controller.checkOtpExpiryTime(optExists)) {
        // TODO: Check whether need to resend non-expired OTP again?
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[220], code: '220', data: [] })
      } else {
        // delete existing otp
        const query = { _id: optExists.id }
        await service.deleteExistingOtp(query)
        // generate new otp, save , send otp
        const type = req.body.type
        if (_.isEmpty(rand)) {
          rand = utils.getRandomNumber(6)
          otpData = {
            code: rand,
            mobile_number: req.body.mobile_number,
            type: type,
            status: STATUS.UN_VERIFIED
          }
          await service.save(otpData)
        }
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[220], code: '220', data: [] })
      }
    } else if (_.isNull(optExists)) {
      // generate new otp, save , send otp
      const type = req.body.type
      if (_.isEmpty(rand)) {
        rand = utils.getRandomNumber(6)
        const otpData = {
          code: rand,
          mobile_number: req.body.mobile_number,
          type: type,
          status: STATUS.UN_VERIFIED
        }
        await service.save(otpData)
      }
      this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[220], code: '220', data: [] })
    }
  }

  static checkOtpExpiryTime (otpData) {
    const currentTime = moment().utc()
    const otpCreateTime = moment(otpData.createdAt)
    const duration = moment.duration(currentTime.diff(otpCreateTime))
    const minutes = duration.asMinutes()
    const expireTime = process.env.OTP_EXPIRES
    return minutes < expireTime
  }

  async verifyOtp (req, res) {
    const otpQuery = {
      mobile_number: req.body.mobile_number,
      type: req.body.type,
      code: req.body.code,
      status: STATUS.UN_VERIFIED
    }
    const optExists = await service.isOtpExist(otpQuery)
    if (!_.isUndefined(optExists) && !_.isNull(optExists) && optExists.id) {
      if (Controller.checkOtpExpiryTime(optExists)) {
        if (req.body.type === 'SIGN_UP') {
          const result = (_.includes[OTP_TYPE.SIGN_UP], req.body.type)
          if (result) {
            const updateParams = {
              status: STATUS.VERIFIED,
              is_verified: '1'
            }
            const otpData = await service.updateOtp({ _id: optExists.id }, updateParams)
            if (!_.isUndefined(otpData) && otpData._id) {
              const userData = await userServ.getUserDetailByMobileNumber({ mobile_number: req.body.mobile_number })
              new AuthController().authenticateSuccessAction(req, res, userData)
            } else {
              this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[431], code: '412' })
            }
          }
        } else if (req.body.type === 'LOGIN') {
          const userData = await userServ.getUserDetailByMobileNumber({ mobile_number: req.body.mobile_number })
          new AuthController().authenticateSuccessAction(req, res, userData)
        }
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[431], code: '412', data: [] })
      }
    } else if (_.isNull(optExists)) {
      this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[431], code: '412', data: [] })
    }
  }

  async list (req, res) {
  }
}

module.exports = Controller
