const mongoose = require('mongoose')
const Schema = mongoose.Schema

const schema = new mongoose.Schema(
  {
    delivery_charge: {
      type: Number
    },
    package_charge: {
      type: Number
    },
    tax_amount: {
      type: Number
    },
    promocodeId: {
      type: Schema.Types.ObjectId,
      ref: 'promocodes'
    },
    total_price: {
      type: Number,
      unique: false
    },
    order_price: {
      type: Number,
      unique: false
    },
    total_products: {
      type: Number
    },
    addressId: {
      type: Schema.Types.ObjectId,
      ref: 'addresses'
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    },
    status: {
      type: String,
      default: 'PENDING',
      enum: ['ORDER_PLACED', 'CONFIRMED', 'ORDER_PROCESSED', 'READY_TO_PICKUP', 'DELIVERY_IN_PROGRESS', 'DELIVERED', 'CANCELLED', 'PENDING', 'PAYMENT_FAILED']
    },
    products: [
      {
        product_count: {
          type: String
        },
        productId: {
          type: String
        },
        price_range: [{
          label: {
            type: String
          },
          stock: {
            type: Number
          },
          is_selected: {
            type: String,
            default: '0',
            enum: ['0', '1']
          },
          value: {
            type: Number
          }
        }]
      }
    ]
  },
  { timestamps: true, versionKey: false }
)

const Order = mongoose.model('orders', schema)
module.exports = Order
