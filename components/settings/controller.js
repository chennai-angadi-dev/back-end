const _ = require('lodash')
const BaseController = require('../base/controller')
const { settingService: service } = require('./service')
const { SUCCESS, ERROR } = require('../../libs/constants')
const { apimsgconfig } = require('../../libs/swagger/api_message')

class Controller extends BaseController {
  async createSetting (req, res, next) {
    try {
      const data = await service.createSetting(req)
      if (!_.isEmpty(data) && data.id) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[202], code: '202', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[599], code: '599' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getSettingDetails (req, res, next) {
    try {
      const result = await service.getSettingDetails(req)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[200], code: '200', data: result })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async updateSettings (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.updateSettings({ _id: id }, req.body)
      if (!_.isEmpty(result) && result.nModified !== 0) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[203], code: '203', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }
}
module.exports = Controller
