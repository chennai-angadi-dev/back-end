module.exports = {
  create: {
    required: [
      'name',
      'mobile_number',
      'email'
    ],
    properties: {
      first_name: {
        type: 'string',
        trim: true
      },
      last_name: {
        type: 'string',
        trim: true
      },
      email: {
        type: 'string',
        trim: true
      },
      mobile_number: {
        type: 'string',
        trim: true
      },
      gender: {
        type: 'string',
        enum: ['MALE', 'FEMALE', 'OTHERS']
      },
      isSocialLogin: {
        type: 'boolean'
      },
      avatar: {
        type: 'string',
        trim: true
      },
      device_type: {
        type: 'string',
        trim: true
      },
      is_verified: {
        type: 'string',
        default: '0',
        enum: ['0', '1']
      },
      device_token: {
        type: 'string',
        trim: true
      },
      last_login: {
        type: 'string',
        trim: true
      },
      socialid: {
        type: 'string',
        trim: true
      },
      role: {
        type: 'string',
        enum: ['SUPERADMIN', 'ADMIN', 'CUSTOMER', 'FINANCE', 'IT']
      },
      enable_notification: {
        type: 'string',
        default: '0',
        enum: ['0', '1']
      },
      status: {
        type: 'string',
        default: 'ACTIVE',
        enum: ['ACTIVE', 'INACTIVE', 'DELETED']
      }
    }
  },
  update: {
    properties: {
      first_name: {
        type: 'string',
        trim: true
      },
      last_name: {
        type: 'string',
        trim: true
      },
      email: {
        type: 'string',
        trim: true
      },
      mobile_number: {
        type: 'string',
        trim: true
      },
      gender: {
        type: 'string',
        enum: ['MALE', 'FEMALE', 'OTHERS']
      },
      isSocialLogin: {
        type: 'boolean'
      },
      avatar: {
        type: 'string',
        trim: true
      },
      device_type: {
        type: 'string',
        trim: true
      },
      is_verified: {
        type: 'string',
        default: '0',
        enum: ['0', '1']
      },
      device_token: {
        type: 'string',
        trim: true
      },
      last_login: {
        type: 'string',
        trim: true
      },
      socialid: {
        type: 'string',
        trim: true
      },
      role: {
        type: 'string',
        enum: ['SUPERADMIN', 'ADMIN', 'CUSTOMER', 'FINANCE', 'IT']
      },
      enable_notification: {
        type: 'string',
        default: '0',
        enum: ['0', '1']
      },
      status: {
        type: 'string',
        default: 'ACTIVE',
        enum: ['ACTIVE', 'INACTIVE', 'DELETED']
      }
    }
  },
  signUp: {
    required: [
      'issocialLogin',
      'mobile_number',
      'role',
      'socialid'
    ],
    properties: {
      mobile_number: {
        type: 'string',
        trim: true
      },
      issocialLogin: {
        type: 'boolean'
      },
      socialid: {
        type: 'string',
        trim: true
      },
      role: {
        type: 'string',
        enum: ['SUPERADMIN', 'ADMIN', 'CUSTOMER', 'FINANCE', 'IT']
      }
    }
  },

  properties: {
    first_name: {
      type: 'string',
      trim: true
    },
    last_name: {
      type: 'string',
      trim: true
    },
    email: {
      type: 'string',
      trim: true
    },
    mobile_number: {
      type: 'string',
      trim: true
    },
    gender: {
      type: 'string',
      enum: ['MALE', 'FEMALE', 'OTHERS']
    },
    isSocialLogin: {
      type: 'boolean'
    },
    avatar: {
      type: 'string',
      trim: true
    },
    device_type: {
      type: 'string',
      trim: true
    },
    is_verified: {
      type: 'string',
      default: '0',
      enum: ['0', '1']
    },
    device_token: {
      type: 'string',
      trim: true
    },
    last_login: {
      type: 'string',
      trim: true
    },
    socialid: {
      type: 'string',
      trim: true
    },
    role: {
      type: 'string',
      enum: ['SUPERADMIN', 'ADMIN', 'CUSTOMER', 'FINANCE', 'IT']
    },
    enable_notification: {
      type: 'string',
      default: '0',
      enum: ['0', '1']
    },
    status: {
      type: 'string',
      default: 'ACTIVE',
      enum: ['ACTIVE', 'INACTIVE', 'DELETED']
    }
  }
}
