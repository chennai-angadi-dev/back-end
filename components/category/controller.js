const _ = require('lodash')
const BaseController = require('../base/controller')
const { categoryService: service } = require('./service')
const { SUCCESS, ERROR } = require('../../libs/constants')
const { apimsgconfig } = require('../../libs/swagger/api_message')

class Controller extends BaseController {
  async createCategory (req, res, next) {
    try {
      const data = await service.createCategory(req.body)
      if (!_.isEmpty(data) && data.id) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[202], code: '202', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[599], code: '599' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getCategories (req, res, next) {
    try {
      const categories = await service.getCategoryAndCount(req)
      if (!_.isEmpty(categories) && !_.isEmpty(categories[0].totalCount)) {
        this.sendResponse(req, res, SUCCESS.CODE, {
          message: apimsgconfig.en_us[200],
          code: '200',
          data: categories && categories[0].results ? categories[0].results : [],
          count: categories && categories[0].totalCount[0] && categories[0].totalCount[0].count
            ? categories[0].totalCount[0].count
            : 0
        })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getCategoryById (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.getById(id)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[200], code: '200', data: result })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async updateCategory (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.updateCategory({ _id: id, status: 'ACTIVE' }, req.body)
      if (!_.isEmpty(result) && result.nModified !== 0) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[203], code: '203', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async deleteCategory (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.deleteCategory({ _id: id }, { status: 'DELETED' })
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[204], code: '204', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }
}
module.exports = Controller
