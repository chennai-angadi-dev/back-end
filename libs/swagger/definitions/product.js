module.exports = {
  create: {
    required: [
      'name',
      'categoryId',
      'subcategoryId',
      'brandId',
      'image',
      'price',
      'rate_per',
      'total_stock',
      'alert_stock'
    ],
    properties: {
      name: {
        type: 'string',
        index: true,
        trim: true,
        unique: true,
        required: [true, 'Name is required.']
      },
      categoryId: {
        type: 'string'
      },
      subcategoryId: {
        type: 'string'
      },
      price: {
        type: 'string'
      },
      description: {
        type: 'string'
      },
      image: {
        type: 'array',
        items: {
          type: 'string'
        }
      },
      price_range: {
        type: 'array',
        items: {
          type: 'string'
        }
      },
      brandId: {
        type: 'string'
      },
      rate_per: {
        type: 'string'
      },
      total_stock: {
        type: 'string'
      },
      alert_stock: {
        type: 'string'
      },
      status: {
        type: 'string',
        default: 'ACTIVE',
        enum: ['ACTIVE', 'ACTIVE', 'DELETED']
      }
    }
  },
  update: {
    properties: {
      name: {
        type: 'string',
        index: true,
        trim: true,
        unique: true,
        required: [true, 'Name is required.']
      },
      categoryId: {
        type: 'string'
      },
      subcategoryId: {
        type: 'string'
      },
      price: {
        type: 'string'
      },
      description: {
        type: 'string'
      },
      image: {
        type: 'array',
        items: {
          type: 'string'
        }
      },
      price_range: {
        type: 'array',
        items: {
          type: 'string'
        }
      },
      brandId: {
        type: 'string'
      },
      rate_per: {
        type: 'string'
      },
      total_stock: {
        type: 'string'
      },
      alert_stock: {
        type: 'string'
      },
      status: {
        type: 'string',
        default: 'ACTIVE',
        enum: ['ACTIVE', 'ACTIVE', 'DELETED']
      }
    }
  },
  properties: {
    name: {
      type: 'string',
      index: true,
      trim: true,
      unique: true,
      required: [true, 'Name is required.']
    },
    categoryId: {
      type: 'string'
    },
    subcategoryId: {
      type: 'string'
    },
    price: {
      type: 'string'
    },
    description: {
      type: 'string'
    },
    image: {
      type: 'array',
      items: {
        type: 'string'
      }
    },
    price_range: {
      type: 'array',
      items: {
        type: 'string'
      }
    },
    brandId: {
      type: 'string'
    },
    rate_per: {
      type: 'string'
    },
    total_stock: {
      type: 'string'
    },
    alert_stock: {
      type: 'string'
    },
    status: {
      type: 'string',
      default: 'ACTIVE',
      enum: ['ACTIVE', 'ACTIVE', 'DELETED']
    }
  }
}
