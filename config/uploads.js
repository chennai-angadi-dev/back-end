const _ = require('lodash')
const { v4: uuidv4 } = require('uuid')
const multer = require('multer')
const mkdirp = require('mkdirp')
const { ALLOWED_MIME_TYPE, ERR_FILE_TYPE } = require('../libs/constants')

const uploadStorage = multer.diskStorage({
  destination: async function (req, file, cb) {
    const type = req.body.type.toLowerCase()
    let prefix = `uploads/${type}`
    let id
    var istypeValid = true
    if (type === 'category') {
      id = `${req.body.categoryId}`
    } else if (type === 'subcategory') {
      id = `${req.body.subcategoryId}`
    } else if (type === 'profile') {
      id = `${req.body.userId}`
    } else if (type === 'product') {
      id = `${req.body.productId}`
    } else {
      istypeValid = false
    }
    if (istypeValid) {
      prefix = `${prefix}/${id}`
      await mkdirp(prefix)
    }
    cb(null, prefix)
  },
  filename: function (req, file, cb) {
    cb(null, `${req.body.type.toLowerCase()}-${uuidv4()}.${file.originalname.split('.')[file.originalname.split('.').length - 1]}`)
  }
})

const uploads = multer({
  storage: uploadStorage,
  limits: { fileSize: `${process.env.MAX_UPLOAD_SIZE_MB}` * 1024 * 1024 },
  fileFilter: function (req, file, cb) {
    if (_.includes(ALLOWED_MIME_TYPE, file.mimetype) === false) {
      cb(new Error(ERR_FILE_TYPE))
    } else {
      cb(null, true)
    }
  }
}).array('file_to_upload', 5)

module.exports = {
  uploads
}
