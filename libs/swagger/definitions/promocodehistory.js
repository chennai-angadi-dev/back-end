module.exports = {
  required: [
    'promo_code_id',
    'orderId',
    'status'
  ],
  create: {
    required: [
      'promo_code_id',
      'orderId',
      'status'
    ],
    properties: {
      promo_code_id: {
        type: 'string',
        index: true,
        trim: true,
        unique: true,
        required: [true, 'Name is required.']
      },
      orderId: {
        type: 'string'
      },
      status: {
        type: 'string',
        default: 'APPLIED',
        enum: ['APPLIED', 'REDEEM']
      }
    }
  },
  update: {
    properties: {
      promo_code_id: {
        type: 'string',
        index: true,
        trim: true,
        unique: true,
        required: [true, 'Name is required.']
      },
      orderId: {
        type: 'string'
      },
      status: {
        type: 'string',
        default: 'APPLIED',
        enum: ['APPLIED', 'REDEEM']
      }
    }
  },
  properties: {
    promo_code_id: {
      type: 'string',
      index: true,
      trim: true,
      unique: true,
      required: [true, 'Name is required.']
    },
    orderId: {
      type: 'string'
    },
    status: {
      type: 'string',
      default: 'APPLIED',
      enum: ['APPLIED', 'REDEEM']
    }
  }
}
