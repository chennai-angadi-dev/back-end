const mongoose = require('mongoose')

const schema = new mongoose.Schema(
  {
    mobile_number: {
      type: String,
      trim: true,
      required: true,
      index: true
    },
    code: {
      type: String,
      trim: true,
      required: true,
      index: true
    },
    type: {
      type: String,
      enum: ['LOGIN', 'SIGN_UP'],
      required: true
    },
    status: {
      type: String,
      default: 'UN_VERIFIED',
      enum: ['UN_VERIFIED', 'VERIFIED', 'EXPIRED']
    },
    is_verified: {
      type: String,
      enum: ['0', '1'],
      default: '0'

    }
  },
  { timestamps: true, versionKey: false }
)

const Otp = mongoose.model('otps', schema)
module.exports = Otp
