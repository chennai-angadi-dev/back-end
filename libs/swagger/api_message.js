module.exports = {
  apimsgconfig: {
    en_us: {
      200: 'Record(s) Found',
      201: 'No record(s) found',
      202: 'Inserted successfully',
      203: 'Updated successfully',
      204: 'Deleted successfully',
      205: 'Feedback sent successfully',
      206: 'Your cart is empty',

      /* user codes */
      211: 'Registered successfully',
      212: 'User verified successfully',
      213: 'Login successfully',
      214: 'Profile updated successfully',
      215: 'User Logged out successfully',

      /* OTP codes */
      220: 'OTP sent successfully',
      221: 'OTP already sent',
      222: 'OTP verified successfully',

      /* orders code */
      231: 'Order items placed successfully',
      232: 'Order placed successfully',
      233: 'Order created successfully',
      234: 'Promocode applied successfully',
      235: 'Promocode removed successfully',
      236: 'Order updated successfully',
      237: 'Address added successfully',
      238: 'Ratings added successfully',
      239: 'Address updated successfully',

      240: 'Product added to wishlist successfully',
      241: 'Product removed from wishlist successfully',
      242: 'Out of stock',

      /* error codes */
      400: 'Invalid request',
      401: 'Invalid auth token',
      402: 'Record not found',
      403: 'Unable to process now, Please try again later',
      404: 'Invalid MIME Type',
      405: 'Token expired/invalid',
      406: "We're facing some issue on sending SMS, please try again",
      407: 'Invalid product data',
      408: 'Please verify mobile number, to login',
      409: 'Something went wrong Please login again to continue',

      /* user codes */
      411: 'Mobile number is required',
      412: 'Invalid mobile number',
      413: 'Mobile number already exist',
      414: 'Mobile number not registered',
      415: 'Login type required',
      416: 'Social id Missing',
      417: 'Social id already exist',

      /* OTP codes */
      430: 'OTP already sent',
      431: 'Invalid/expired OTP',
      432: 'OTP is required',
      433: 'OTP type is required',
      434: 'Product ID is required',
      435: 'Order ID is required',

      /* order codes */
      441: 'Invalid customer data',
      442: 'Invalid order data',
      443: 'Payment method is required',
      444: 'Address is required',
      445: 'Please enter Promo Code',
      446: 'Order is required',
      463: 'Invalid Promocode',
      464: 'Promo code is expired',
      465: 'Promo code is already used',
      466: 'Order amount low for this promocode',
      471: 'Promocode not available',
      472: 'Order status updated successfully',

      /* address codes */
      511: 'Flat number is required',
      512: 'Area is required',
      513: 'Street name is required',
      514: 'Building name is required',
      515: 'Invalid address data',

      /* rating */
      521: 'Rating is required',
      522: 'Rating type is required',
      523: 'Review title is required',
      524: 'Invalid rating images',

      /* feedback */
      531: 'Subject is required',
      532: 'Description is required',
      533: 'Feedback type is required',

      /* Notification */
      541: 'Notification setting updated successfully',

      /* image upload */
      551: 'Something went wrong Image not uploaded',

      /* Payments */
      561: 'Invalid payment method',

      599: 'Internal server error'

      // 202: 'Required username'
    }
  }

}
