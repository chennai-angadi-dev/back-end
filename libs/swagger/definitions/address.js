module.exports = {
  required: [
    'flat_no',
    'building_name',
    'street',
    'area',
    'city',
    'pincode',
    'landmark',
    'is_default',
    'state'
  ],
  create: {
    required: [
      'flat_no',
      'building_name',
      'street',
      'area',
      'city',
      'pincode',
      'landmark',
      'is_default',
      'state'
    ],
    properties: {
      flat_no: {
        type: 'string'
      },
      state: {
        type: 'string'
      },
      building_name: {
        type: 'string'
      },
      street: {
        type: 'string'
      },
      area: {
        type: 'string'
      },
      city: {
        type: 'string'
      },
      pincode: {
        type: 'number'
      },
      landmark: {
        type: 'string'
      },
      is_default: {
        type: 'string',
        enum: ['0', '1']
      }
    }
  },
  update: {
    properties: {
      flat_no: {
        type: 'string'
      },
      state: {
        type: 'string'
      },
      building_name: {
        type: 'string'
      },
      street: {
        type: 'string'
      },
      area: {
        type: 'string'
      },
      city: {
        type: 'string'
      },
      pincode: {
        type: 'number'
      },
      landmark: {
        type: 'string'
      },
      is_default: {
        type: 'string',
        enum: ['0', '1']
      }
    }
  },
  properties: {
    flat_no: {
      type: 'string'
    },
    state: {
      type: 'string'
    },
    building_name: {
      type: 'string'
    },
    street: {
      type: 'string'
    },
    area: {
      type: 'string'
    },
    city: {
      type: 'string'
    },
    pincode: {
      type: 'number'
    },
    landmark: {
      type: 'string'
    },
    is_default: {
      type: 'string',
      enum: ['0', '1']
    }
  }
}
