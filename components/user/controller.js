const _ = require('lodash')
const jwt = require('jsonwebtoken')
const BaseController = require('../base/controller')
const { userService: service } = require('./service')
const { SUCCESS, ERROR, STATUS } = require('../../libs/constants')
const { apimsgconfig } = require('../../libs/swagger/api_message')
const utils = require('../../helpers/utils')

class Controller extends BaseController {
  async createUser (req, res, next) {
    try {
      const mobileNumber = req.body.mobile_number
      const mob = /^[1-9]{1}[0-9]{9}$/
      if (!mob.test(mobileNumber)) {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[412], code: '412' })
        return false
      }
      const data = await service.createUser(req.body)
      if (!_.isEmpty(data) && data._id) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[202], code: '202', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[400], code: '400' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getUsers (req, res, next) {
    try {
      const result = await service.getUserAndCount(req)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[200], code: '200', data: result.data, count: result.count })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getUserById (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.getById(id)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[200], code: '200', data: result })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async updateUser (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.updateUser({ _id: id, status: 'ACTIVE' }, req.body)
      if (!_.isEmpty(result) && result.nModified !== 0) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[203], code: '203', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[599], code: '599' })
      }
    } catch (e) {
      next(e)
    }
  }

  async deleteUser (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.deleteUser({ _id: id }, { status: 'DELETED' })
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[204], code: '204', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[599], code: '599' })
      }
    } catch (e) {
      next(e)
    }
  }

  async sendOtp (req) {
    const rand = utils.getRandomNumber(6)
    const otpData = {
      code: rand,
      mobile_number: req.body.mobile_number,
      type: 'SIGN_UP',
      status: STATUS.UN_VERIFIED
    }
    return await service.save(otpData)
  }

  static generateToken (payload) {
    if (typeof payload !== 'object' || payload.constructor !== Object) {
      throw new Error('Payload should be of type object')
    }
    const secret = process.env.JWT_SECRET
    const expiresIn = process.env.JWT_EXPIRES_IN
    return jwt.sign(payload, secret, { expiresIn })
  }

  static getPayload (data) {
    return {
      id: data._id,
      mobile_number: data.mobile_number,
      role: data.role,
      username: data.username,
      status: data.status
    }
  }

  async signUp (req, res, next) {
    try {
      const isSocialLogin = req.body.isSocialLogin
      if (isSocialLogin === true) {
        if (!req.body.socialid) {
          this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[416], code: '416' })
        } else {
          const socialIdExist = await service.verifysocialid(req)
          if (socialIdExist) {
            this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[417], code: '417' })
          } else {
            if (!_.isNull(req.body.mobile_number) && !_.isUndefined(req.body.mobile_number)) {
              const userData = await service.getUserDetailByMobileNumber({ mobile_number: req.body.mobile_number })
              if (userData) {
                this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[413], code: '413' })
                return
              }
            }
            const createdUser = await service.createUser(req.body)
            if (createdUser) {
              const user = await service.verifyAdminUser(req.body)
              if (_.isEmpty(user) && !user._id) {
                return
              }
              const payload = Controller.getPayload(user)
              const token = Controller.generateToken(payload)
              if (token) {
                const decoded = jwt.decode(token)
                this.sendResponse(req, res, SUCCESS.CODE, {
                  code: 213,
                  data: {
                    token,
                    expired_time: decoded.exp
                  },
                  message: apimsgconfig.en_us[213]
                })
              } else {
                this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[400], code: '400' })
              }
            } else {
              this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[400], code: '400' })
            }
          }
        }
      } else {
        const mobileNumber = req.body.mobile_number
        const mob = /^[1-9]{1}[0-9]{9}$/
        if (!mob.test(mobileNumber)) {
          this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[412], code: '412' })
          return
        }
        const userData = await service.getUserDetailByMobileNumber({ mobile_number: req.body.mobile_number })
        if (userData) {
          this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[413], code: '413' })
          return
        }
        const sentOtp = await this.sendOtp(req)
        const createdUser = await service.createUser(req.body)
        if (sentOtp && createdUser) {
          this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[220], code: '220', data: [] })
        } else {
          this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[400], code: '400' })
        }
      }
    } catch (e) {
      next(e)
    }
  }

  async getProfile (req, res, next) {
    try {
      const { id } = req.user
      const result = await service.getById(id)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[200], data: { id: result.id, email: result.email, mobile_number: result.mobile_number, gender: result.gender, enable_notification: result.enable_notification, avatar: result.avatar }, code: '200' })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[400], code: '400' })
      }
    } catch (e) {
      next(e)
    }
  }

  async updateProfile (req, res, next) {
    try {
      const { id } = req.user
      const result = await service.updateUser({ _id: id }, req.body)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[214], code: '214', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[413], code: '413' })
      }
    } catch (e) {
      next(e)
    }
  }

  async userLogOut (req, res, next) {
    try {
      const { id } = req.user
      const updateParams = { device_token: null }
      const userFromDB = await service.getById(id)
      const result = await service.updateUser({ _id: id }, updateParams)
      if (!_.isEmpty(result) && userFromDB) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[215], code: '215', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[400], code: '400' })
      }
    } catch (e) {
      next(e)
    }
  }
}
module.exports = Controller
