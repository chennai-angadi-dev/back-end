const joi = require('@hapi/joi')

const read = joi.object().keys()

const list = joi.object().keys()

const create = joi.object().keys({
  notify_type: joi.string().required(),
  notify_offer: joi.string().required(),
  notify_promo: joi.string().required(),
  userId: joi.string().required().required(),
  number_of_orders: joi.string().required(),
  message: joi.string().required(),
  status: joi.string().required(),
  sent_date: joi.string().required(),
  schedule_date: joi.string().required()
})

const enable = joi.object().keys({
  enable_notification: joi.string().required()
})

const update = joi.object().keys({
  notify_type: joi.string(),
  notify_offer: joi.string(),
  notify_promo: joi.string(),
  userId: joi.string().required(),
  number_of_orders: joi.string(),
  message: joi.string(),
  status: joi.string(),
  sent_date: joi.string(),
  schedule_date: joi.string()
})

const remove = joi.object().keys()

module.exports = {
  list,
  read,
  create,
  enable,
  update,
  remove
}
