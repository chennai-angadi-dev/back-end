const Product = require('./schema')
const { errLogger } = require('../../config/logger')
const Offer = require('../offers/schema')
const Brands = require('../brands/schema')
const Category = require('../category/schema')
const Subcategory = require('../subcategory/schema')

class Service {
  async createProduct (params) {
    const data = new Product(params)
    return data.save().catch((e) => {
      errLogger.error({ method: 'Product-save', message: e.message })
    })
  }

  getProductAndCount (req) {
    const reqQuery = req.query
    const limit = Number(
      reqQuery && reqQuery.limit ? reqQuery.limit : process.env.PAGE_LIMIT
    )
    const pageNo = Number(reqQuery && reqQuery.page ? limit * reqQuery.page - limit : 0)
    const sortField = reqQuery && reqQuery.sort ? reqQuery.sort : 'createdAt'
    const order = reqQuery && reqQuery.order && reqQuery.order === 'asc' ? 1 : -1
    const match = { $and: [] }

    if (reqQuery.status) {
      match.$and.push({ status: reqQuery.status })
    } else {
      match.$and.push({ status: 'ACTIVE' })
    }

    if (reqQuery.name) {
      match.$and.push({ name: { $regex: reqQuery.name, $options: 'i' } })
    }

    if (reqQuery.subcategory) {
      match.$and.push({ 'subcategoryId.name': { $regex: reqQuery.subcategory, $options: 'i' } })
    }

    if (reqQuery.category) {
      match.$and.push({ 'categoryId.name': { $regex: reqQuery.category, $options: 'i' } })
    }

    if (reqQuery.brandId) {
      match.$and.push({ 'brandId.name': { $regex: reqQuery.category, $options: 'i' } })
    }

    return Product.aggregate([
      {
        $lookup: {
          from: 'categories',
          let: { id: '$categoryId' },
          pipeline: [
            { $match: { $expr: { $eq: ['$_id', '$$id'] } } },
            { $project: { name: 1 } }
          ],
          as: 'categoryId'
        }
      },
      {
        $unwind: { path: '$categoryId', preserveNullAndEmptyArrays: true }
      },
      {
        $lookup: {
          from: 'subcategories',
          let: { id: '$subcategoryId' },
          pipeline: [
            { $match: { $expr: { $eq: ['$_id', '$$id'] } } },
            { $project: { name: 1 } },
            {
              $lookup: {
                from: 'categories',
                let: { id: '$categoryId' },
                pipeline: [
                  { $match: { $expr: { $eq: ['$_id', '$$id'] } } },
                  { $project: { name: 1 } }
                ],
                as: 'categoryId'
              }
            },
            {
              $unwind: { path: '$categoryId', preserveNullAndEmptyArrays: true }
            }
          ],
          as: 'subcategoryId'
        }
      },
      {
        $unwind: { path: '$subcategoryId', preserveNullAndEmptyArrays: true }
      },
      {
        $lookup: {
          from: 'brands',
          let: { id: '$brandId' },
          pipeline: [
            { $match: { $expr: { $eq: ['$_id', '$$id'] } } },
            { $project: { name: 1 } }
          ],
          as: 'brandId'
        }
      },
      {
        $unwind: { path: '$brandId', preserveNullAndEmptyArrays: true }
      },
      { $project: { name: 1, categoryId: 1, subcategoryId: 1, brandId: 1, image: 1, price: 1, price_range: 1, description: 1, rate_per: 1, total_stock: 1, alert_stock: 1, status: 1 } },
      {
        $match: match
      },
      {
        $facet: {
          results: [
            { $sort: { [`${sortField}`]: order } },
            { $skip: pageNo },
            { $limit: limit }
          ],
          totalCount: [
            {
              $count: 'count'
            }
          ]
        }
      }
    ])
  }

  async filterData (req) {
    const reqData = req.query
    var products = null
    if (reqData.category) {
      products = await Product.find({ categoryId: this.splitStringToArray(reqData.category), status: 'ACTIVE' }).lean().catch((e) => {
        errLogger.error({ method: 'productcat-getById', message: e.message })
      })
    }

    var products1 = null
    if (reqData.brands) {
      products1 = await Product.find({ brandId: this.splitStringToArray(reqData.brands), status: 'ACTIVE' }).lean()
    }

    var products2 = null
    var products3 = null
    if (reqData.price) {
      const priceData = this.splitStringToArray(reqData.price)
      if (priceData[0]) {
        products2 = await Product.find({ price: { $gte: priceData[0] }, status: 'ACTIVE' }).lean()
      }
      if (priceData[1]) {
        products3 = await Product.find({ price: { $lte: priceData[1] }, status: 'ACTIVE' }).lean()
      }
    }

    var productIds = null
    productIds = this.GetValidProductIds(products, productIds)
    productIds = this.GetValidProductIds(products1, productIds)
    productIds = this.GetValidProductIds(products2, productIds)
    productIds = this.GetValidProductIds(products3, productIds)

    var offers = null
    if (reqData.offer) {
      offers = (await Offer.find({ _id: this.splitStringToArray(reqData.offer), status: 'ACTIVE' }).lean())
    }
    const queryProd = {}
    if (productIds != null) {
      queryProd._id = productIds
    }
    var consolidatedProducts = await Product.find(queryProd).lean()
    var resultProducts = []
    for (var prod of consolidatedProducts) {
      prod.categoryId = await Category.findOne({ _id: prod.categoryId }).select('name').lean()
      prod.subcategoryId = await Subcategory.findOne({ _id: prod.subcategoryId }).select('name').lean()
      prod.brandId = await Brands.findOne({ _id: prod.brandId }).select('name').lean()
      if (offers != null) {
        const availableOffer = this.findInArrayObj(prod._id, offers)
        if (availableOffer != null) {
          prod.offer = this.findInArrayObj(prod._id, offers)
          resultProducts.push(prod)
        }
      } else {
        resultProducts.push(prod)
      }
    }
    return resultProducts
  }

  splitStringToArray (stringData) {
    stringData = stringData.replace('[', '').replace(']', '').trim()
    return stringData.split(',')
  }

  findInArrayObj (prodId, myArray) {
    for (var i = 0; i < myArray.length; i++) {
      if ((myArray[i].productId).equals(prodId)) {
        return myArray[i]
      }
    }
    return null
  }

  findInArray (prodId, myArray) {
    for (var i = 0; i < myArray.length; i++) {
      if ((myArray[i]).equals(prodId)) {
        return myArray[i]
      }
    }
    return null
  }

  GetValidProductIds (products, productIds) {
    var filteredProductIds = []
    if (products) {
      products.forEach(elem => {
        if (productIds == null) {
          filteredProductIds.push(elem._id)
        } else {
          if (this.findInArray(elem._id, productIds) != null) {
            filteredProductIds.push(elem._id)
          }
        }
      })
    } else {
      filteredProductIds = productIds
    }
    return filteredProductIds
  }

  async getAll () {
    return Product.find()
      .populate({
        path: 'categoryId',
        select: 'name'
      })
      .populate({
        path: 'subcategoryId',
        select: 'name',
        populate: {
          path: 'categoryId',
          select: 'name'
        }
      })
      .sort({ createdAt: -1 }).lean().catch((e) => {
        errLogger.error({ method: 'Product-getAll', message: e.message })
      })
  }

  async getById (id) {
    return Product.findById(id)
      .populate({
        path: 'categoryId',
        select: 'name'
      })
      .populate({
        path: 'subcategoryId',
        select: 'name'
      })
      .populate({
        path: 'brandId',
        select: 'name'
      })
      .catch((e) => {
        errLogger.error({ method: 'Product-getById', message: e.message })
      })
  }

  async updateProduct (id, updateParams) {
    return Product.updateOne(id, updateParams).catch((e) => {
      errLogger.error({ method: 'Product-getById', message: e.message })
    })
  }

  async deleteProduct (id, updateParams) {
    return Product.updateOne(id, updateParams).catch((e) => {
      errLogger.error({ method: 'Product-getById', message: e.message })
    })
  }
}

const productService = new Service()

module.exports = { Service, productService }
