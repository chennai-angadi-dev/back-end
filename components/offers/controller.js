const _ = require('lodash')
const BaseController = require('../base/controller')
const { offerService: service } = require('./service')
const { SUCCESS, ERROR } = require('../../libs/constants')
const { apimsgconfig } = require('../../libs/swagger/api_message')

class Controller extends BaseController {
  async createOffer (req, res, next) {
    try {
      const result = await service.createOffer(req.body)
      if (result === 202) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[202], code: '202', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[result], code: result.toString() })
      }
    } catch (e) {
      next(e)
    }
  }

  async getOffers (req, res, next) {
    try {
      const offers = await service.getOfferAndCount(req)
      if (!_.isEmpty(offers)) {
        const count = (offers && offers[0].totalCount[0] && offers[0].totalCount[0].count
          ? offers[0].totalCount[0].count : 0)
        var resultMsgIdx = count === 0 ? 201 : 200
        this.sendResponse(req, res, SUCCESS.CODE, {
          message: apimsgconfig.en_us[resultMsgIdx],
          code: resultMsgIdx.toString(),
          data: offers && offers[0].results ? offers[0].results : [],
          count: count
        })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getOfferById (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.getById(id)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[200], code: '200', data: result })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async updateOffer (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.updateOffer({ _id: id, status: 'ACTIVE' }, req.body)
      if (!_.isEmpty(result) && result.nModified !== 0) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[203], code: '203', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[599], code: '599' })
      }
    } catch (e) {
      next(e)
    }
  }

  async deleteOffer (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.deleteOffer({ _id: id }, { status: 'DELETED' })
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[204], code: '204', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }
}

module.exports = Controller
