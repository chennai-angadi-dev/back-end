const joi = require('@hapi/joi')

const read = joi.object().keys()

const list = joi.object().keys()

const create = joi.object().keys({
  orderId: joi.string().required(),
  order_status: joi.string().required()
})

module.exports = {
  list,
  read,
  create
}
