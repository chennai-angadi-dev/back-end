const authentication = require('./authentication')
const user = require('./user')
const otp = require('./otp')
const appConfig = require('./appConfig')
const headers = require('./header')
const response = require('./response')
const subcategory = require('./subcategory')
const category = require('./category')
const promocode = require('./promocode')
const offers = require('./offers')
const brands = require('./brands')
const wishlist = require('./wishlist')
const address = require('./address')
const product = require('./product')
const orders = require('./orders')
const notifications = require('./notifications')
const promocodehistory = require('./promocodehistory')
const orderhistory = require('./orderhistory')
const settings = require('./settings')

module.exports = {
  Auth: authentication,
  User: user,
  AppConfig: appConfig,
  Headers: headers,
  Request: response,
  Response: response,
  Otp: otp,
  Subcategory: subcategory,
  Category: category,
  Promocode: promocode,
  Offers: offers,
  Brands: brands,
  Wishlist: wishlist,
  Address: address,
  Product: product,
  Orders: orders,
  Promocodehistory: promocodehistory,
  Settings: settings,
  Orderhistory: orderhistory,
  Notifications: notifications
}
