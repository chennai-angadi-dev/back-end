const mongoose = require('mongoose')
const Schema = mongoose.Schema

const schema = new Schema(
  {
    first_name: {
      type: String,
      trim: true
    },
    last_name: {
      type: String,
      trim: true
    },
    email: {
      type: String,
      trim: true
    },
    mobile_number: {
      type: String,
      trim: true
    },
    addressId: {
      type: Schema.Types.ObjectId,
      ref: 'addresses'
    },
    gender: {
      type: String,
      enum: ['MALE', 'FEMALE', 'OTHERS']
    },
    password: {
      type: String,
      trim: true
    },
    avatar: {
      type: String,
      trim: true
    },
    device_type: {
      type: String,
      trim: true
    },
    is_verified: {
      type: String,
      default: '0',
      enum: ['0', '1']
    },
    device_token: {
      type: String,
      trim: true
    },
    last_login: {
      type: String,
      trim: true
    },
    socialid: {
      type: String,
      trim: true
    },
    isSocialLogin: {
      type: Boolean
    },
    role: {
      type: String,
      enum: ['SUPERADMIN', 'ADMIN', 'CUSTOMER', 'FINANCE', 'IT']
    },
    enable_notification: {
      type: String,
      default: '0',
      enum: ['0', '1']
    },

    status: {
      type: String,
      default: 'ACTIVE',
      enum: ['ACTIVE', 'INACTIVE', 'DELETED']
    }
  },
  { timestamps: true, versionKey: false }
)

const User = mongoose.model('users', schema)
module.exports = User
