module.exports = {
  create: {
    required: [
      'name',
      'code',
      'description',
      'type',
      'discount_value',
      'valid_from',
      'valid_to',
      'minimum_order_amount',
      'maximum_number_of_users',
      'image',
      'status'
    ],
    properties: {
      name: {
        type: 'string'
      },
      code: {
        type: 'string'
      },
      description: {
        type: 'string'
      },
      type: {
        type: 'string',
        default: 'Select type',
        enum: ['Flat', '%']
      },
      discount_value: {
        type: 'number'
      },
      valid_from: {
        type: 'string',
        format: 'date'
      },
      valid_to: {
        type: 'string',
        format: 'date'
      },
      minimum_order_amount: {
        type: 'number'
      },
      maximum_number_of_users: {
        type: 'number'
      },
      image: {
        type: 'string'
      },
      status: {
        type: 'string',
        enum: ['ACTIVE', 'INACTIVE', 'DELETED']
      }
    }
  },
  update: {
    properties: {
      name: {
        type: 'string'
      },
      code: {
        type: 'string'
      },
      description: {
        type: 'string'
      },
      type: {
        type: 'string',
        enum: ['Flat', '%']
      },
      discount_value: {
        type: 'number'
      },
      valid_from: {
        type: 'string',
        format: 'date'
      },
      valid_to: {
        type: 'string',
        format: 'date'
      },
      minimum_order_amount: {
        type: 'number'
      },
      maximum_number_of_users: {
        type: 'number'
      },
      image: {
        type: 'string'
      },
      status: {
        type: 'string',
        enum: ['ACTIVE', 'INACTIVE', 'DELETED']
      }
    }
  }
}
