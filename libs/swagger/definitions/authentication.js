module.exports = {
  authenticate: {
    required: [
      'mobile_number',
      'isSocialLogin',
      'socialid'
    ],
    properties: {
      mobile_number: {
        type: 'string'
      },
      isSocialLogin: {
        type: 'boolean'
      },
      socialid: {
        type: 'string'
      }
    }
  }
}
