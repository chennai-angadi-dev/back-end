const _ = require('lodash')
const BaseController = require('../base/controller')
const { subcategoryService: service } = require('./service')
const { SUCCESS, ERROR } = require('../../libs/constants')
const { apimsgconfig } = require('../../libs/swagger/api_message')

class Controller extends BaseController {
  async createSubcategory (req, res, next) {
    try {
      const data = await service.createSubcategory(req.body)
      if (!_.isEmpty(data) && data.id) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[202], code: '202', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[599], code: '599' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getSubcategories (req, res, next) {
    try {
      const subcategories = await service.getSubcategoryAndCount(req)
      if (!_.isEmpty(subcategories) && !_.isEmpty(subcategories[0].totalCount)) {
        this.sendResponse(req, res, SUCCESS.CODE, {
          message: apimsgconfig.en_us[200],
          code: '200',
          data: subcategories && subcategories[0].results ? subcategories[0].results : [],
          count: subcategories && subcategories[0].totalCount[0] && subcategories[0].totalCount[0].count
            ? subcategories[0].totalCount[0].count
            : 0
        })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getSubcategoryById (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.getById(id)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[200], code: '200', data: result })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async updateSubcategory (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.updateSubcategory({ _id: id, status: 'ACTIVE' }, req.body)
      if (!_.isEmpty(result) && result.nModified !== 0) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[203], code: '203', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async deleteSubcategory (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.deleteSubcategory({ _id: id }, { status: 'DELETED' })
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[204], code: '204', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }
}

module.exports = Controller
