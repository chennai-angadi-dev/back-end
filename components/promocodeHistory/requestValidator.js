const joi = require('@hapi/joi')

const list = joi.object().keys()

const apply = joi.object().keys({
  code: joi.string().required(),
  orderId: joi.string().required(),
  status: joi.string()
})

const update = joi.object().keys({
  status: joi.string()
})

module.exports = {
  apply,
  update,
  list

}
