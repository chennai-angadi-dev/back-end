const _ = require('lodash')
const BaseController = require('../base/controller')
const { promocodeService: service } = require('./service')
const { SUCCESS, ERROR } = require('../../libs/constants')
const { apimsgconfig } = require('../../libs/swagger/api_message')

class Controller extends BaseController {
  async createPromocode (req, res, next) {
    try {
      const result = await service.createPromocode(req.body)
      if (result === 202) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[202], code: '202', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[result], code: result.toString() })
      }
    } catch (e) {
      next(e)
    }
  }

  async getPromocodes (req, res, next) {
    try {
      const promocodes = await service.getPromocodeAndCount(req)
      if (!_.isEmpty(promocodes)) {
        const count = (promocodes && promocodes[0].totalCount[0] && promocodes[0].totalCount[0].count
          ? promocodes[0].totalCount[0].count : 0)
        var resultMsgIdx = count === 0 ? 201 : 200
        this.sendResponse(req, res, SUCCESS.CODE, {
          message: apimsgconfig.en_us[resultMsgIdx],
          code: resultMsgIdx.toString(),
          data: promocodes && promocodes[0].results ? promocodes[0].results : [],
          count: count
        })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getPromocodeById (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.getById(id)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[200], code: '200', data: result })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async updatePromocode (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.updatePromocode({ _id: id }, req.body)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[203], code: '203', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async deletePromocode (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.deletePromocode({ _id: id }, { status: 'DELETED' })
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[204], code: '204', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }
}

module.exports = Controller
