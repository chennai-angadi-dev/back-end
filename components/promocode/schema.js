const mongoose = require('mongoose')
const schema = new mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
      required: [true, 'Name is required.']
    },
    code: {
      type: String,
      trim: true,
      required: true
    },
    description: {
      type: String,
      trim: true
    },
    type: {
      type: String,
      enum: ['Flat', '%']
    },
    discount_value: {
      type: Number,
      trim: true,
      required: true
    },
    valid_from: {
      type: Date,
      trim: true,
      required: true
    },
    valid_to: {
      type: Date,
      trim: true,
      required: true
    },
    minimum_order_amount: {
      type: Number,
      trim: true,
      required: true
    },
    maximum_number_of_users: {
      type: Number,
      trim: true,
      required: true
    },
    image: {
      type: String,
      trim: true,
      required: true
    },
    status: {
      type: String,
      default: 'ACTIVE',
      enum: ['ACTIVE', 'INACTIVE', 'DELETED ']
    }
  },
  { timestamps: true, versionKey: false }
)

const Promocodes = mongoose.model('promocodes', schema)
module.exports = Promocodes
