const joi = require('@hapi/joi')

const read = joi.object().keys()

const list = joi.object().keys()

const create = joi.object().keys({
  name: joi.string().required(),
  status: joi.string(),
  categoryId: joi.string().required()
})

const update = joi.object().keys()

const remove = joi.object().keys()

module.exports = {
  list,
  read,
  create,
  update,
  remove
}
