const Offer = require('./schema')
const { errLogger } = require('../../config/logger')

class Service {
  async createOffer (params) {
    var count = await Offer.count({ name: params.name, status: { $ne: 'DELETED' } })
    if (count > 0) {
      return 400
    }
    const data = new Offer(params)
    return (data.save().catch((e) => {
      errLogger.error({ method: 'offer-save', message: e.message })
    }) ? 202 : 599)
  }

  getOfferAndCount (req) {
    var reqQuery = req.query
    const limit = Number(
      reqQuery && reqQuery.limit ? reqQuery.limit : process.env.PAGE_LIMIT
    )
    const pageNo = Number(reqQuery && reqQuery.page ? limit * reqQuery.page - limit : 0)
    const sortField = reqQuery && reqQuery.sort ? reqQuery.sort : 'createdAt'
    const order = reqQuery && reqQuery.order && reqQuery.order === 'asc' ? 1 : -1
    const match = { $and: [] }

    if (reqQuery.status) {
      match.$and.push({ status: reqQuery.status })
    } else {
      match.$and.push({ status: 'ACTIVE' })
    }

    if (reqQuery.name) {
      match.$and.push({ name: { $regex: reqQuery.name, $options: 'i' } })
    }

    if (reqQuery.valid_from) {
      match.$and.push({ valid_from: { $lte: new Date(reqQuery.valid_from) } })
    }

    if (reqQuery.valid_to) {
      match.$and.push({ valid_to: { $gte: new Date(reqQuery.valid_to) } })
    }

    return Offer.aggregate([
      {
        $lookup: {
          from: 'categories',
          let: { id: '$categoryId' },
          pipeline: [
            { $match: { $expr: { $eq: ['$_id', '$$id'] } } },
            { $project: { name: 1 } }
          ],
          as: 'categoryId'
        }
      },
      {
        $unwind: { path: '$categoryId', preserveNullAndEmptyArrays: true }
      },
      {
        $lookup: {
          from: 'products',
          let: { id: '$productId' },
          pipeline: [
            { $match: { $expr: { $eq: ['$_id', '$$id'] } } },
            { $project: { name: 1 } },
            {
              $lookup: {
                from: 'categories',
                let: { id: '$categoryId' },
                pipeline: [
                  { $match: { $expr: { $eq: ['$_id', '$$id'] } } },
                  { $project: { name: 1 } }
                ],
                as: 'categoryId'
              }
            },
            {
              $unwind: { path: '$categoryId', preserveNullAndEmptyArrays: true }
            }
          ],
          as: 'productId'
        }
      },
      {
        $unwind: { path: '$productId', preserveNullAndEmptyArrays: true }
      },
      { $project: { name: 1, categoryId: 1, productId: 1, description: 1, type: 1, discount_value: 1, valid_from: 1, valid_to: 1, maximum_discount_amount: 1, minimum_order_amount: 1, image: 1, status: 1 } },
      {
        $match: match
      },
      {
        $facet: {
          results: [
            { $sort: { [`${sortField}`]: order } },
            { $skip: pageNo },
            { $limit: limit }
          ],
          totalCount: [
            {
              $count: 'count'
            }
          ]
        }
      }
    ])
  }

  async getAll () {
    return Offer.find()
      .populate({
        path: 'categoryId',
        select: 'name'
      })
      .populate({
        path: 'productId',
        select: 'name',
        populate: {
          path: 'categoryId',
          select: 'name'
        }
      })
      .select('name status categoryId productId')
      .sort({ createdAt: -1 }).lean().catch((e) => {
        errLogger.error({ method: 'Offer-getAll', message: e.message })
      })
  }

  async getById (id) {
    return Offer.findById(id)
      .populate({
        path: 'categoryId',
        select: 'name'
      })
      .populate({
        path: 'productId',
        select: 'name'
      })

      .select('name status categoryId productId brandId')
      .catch((e) => {
        errLogger.error({ method: 'Offer-getById', message: e.message })
      })
  }

  async updateOffer (id, updateParams) {
    return Offer.updateOne(id, updateParams).catch((e) => {
      errLogger.error({ method: 'Offer-getById', message: e.message })
    })
  }

  async deleteOffer (id, updateParams) {
    return Offer.updateOne(id, updateParams).catch((e) => {
      errLogger.error({ method: 'Offer-getById', message: e.message })
    })
  }
}

const offerService = new Service()

module.exports = { Service, offerService }
