module.exports = {
  generateOtp: {
    required: ['mobile_number', 'type'],
    properties: {
      mobile_number: {
        type: 'string'
      },
      type: {
        type: 'string',
        enum: ['LOGIN']
      }
    }
  },
  sendOtp: {
    required: ['mobile_number', 'type'],
    properties: {
      mobile_number: {
        type: 'string'
      },
      type: {
        type: 'string',
        enum: ['LOGIN', 'SIGN_UP', 'SIGNIN']
      }
    }
  },
  verifyOtp: {
    required: ['mobile_number', 'type', 'code'],
    properties: {
      mobile_number: {
        type: 'string'
      },
      type: {
        type: 'string',
        enum: ['LOGIN', 'SIGN_UP', 'SIGNIN']
      },
      code: {
        type: 'string'
      }
    }
  },
  properties: {
    mobile_number: {
      type: 'string'
    },
    type: {
      type: 'string',
      enum: ['LOGIN', 'SIGN_UP', 'SIGNIN']
    },
    code: {
      type: 'string'
    },
    status: {
      type: 'string',
      enum: ['UN_VERIFIED', 'VERIFIED', 'EXPIRED']

    }
  }
}
