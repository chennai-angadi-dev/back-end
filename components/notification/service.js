const Notification = require('./schema')
const { errLogger } = require('../../config/logger')
const Users = require('../user/schema')
const Order = require('../orders/schema')

class Service {
  async createNotification (req) {
    var users = await Users.find({ enable_notification: '1' }).lean()
    var numberOfOrders = req.body.number_of_orders
    for (var user of users) {
      var usercount = await Order.countDocuments({ userId: user._id })
      if (usercount >= numberOfOrders) {
        const data = new Notification(req.body)
        return data.save().catch((e) => {
          errLogger.error({ method: 'notification-save all', message: e.message })
        })
      } else {
        return null
      }
    }
  }

  async getNotificationAndCount (req) {
    var reqQuery = req.query
    const limit = Number(
      reqQuery && reqQuery.limit ? reqQuery.limit : process.env.PAGE_LIMIT
    )
    const pageNo = Number(reqQuery && reqQuery.page ? limit * reqQuery.page - limit : 0)
    const sortField = reqQuery && reqQuery.sort ? reqQuery.sort : 'createdAt'
    const order = reqQuery && reqQuery.order && reqQuery.order === 'asc' ? 1 : -1
    const match = { $and: [] }
    var findQuery = null
    if (reqQuery.status) {
      match.$and.push({ status: reqQuery.status, userId: req.user.id })
    } else {
      match.$and.push({ status: 'ACTIVE' })
    }
    match.$and.push({ userId: req.user.id })

    if (reqQuery.status) {
      findQuery = { notify_type: reqQuery.notify_type, userId: req.user.id }
    } else { findQuery = { userId: req.user.id } }
    const notifyData = (await Notification.find(findQuery).select('message').populate({
      path: '_id,_id.message'
    }).populate({
      path: '_id,_id.message',
      options: {
        sort: { [`${sortField}`]: order },
        skip: pageNo,
        limit: limit
      }
    }).lean())
    return { data: notifyData, count: notifyData ? notifyData.length : 0 }
  }

  async getById (id) {
    return Notification.findById(id).select('message').lean().catch((e) => {
      errLogger.error({ method: 'Notification-getById', message: e.message })
    })
  }

  async enableNotification (id, updateParams) {
    return Users.updateOne(id, updateParams).catch((e) => {
      errLogger.error({ method: 'Notification-enable', message: e.message })
    })
  }

  async updateNotification (id, updateParams) {
    return Notification.updateOne(id, updateParams).catch((e) => {
      errLogger.error({ method: 'Notification-getById', message: e.message })
    })
  }

  async deleteNotification (id) {
    return Notification.remove(id).catch((e) => {
      errLogger.error({ method: 'Notification-getById', message: e.message })
    })
  }
}

const notificationService = new Service()

module.exports = { Service, notificationService }
