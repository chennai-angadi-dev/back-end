const mongoose = require('mongoose')
const Schema = mongoose.Schema

const schema = new mongoose.Schema(
  {
    notify_type: {
      type: String,
      enum: ['Promocodes', 'Offers', 'Others']
    },
    notify_offer: {
      type: Schema.Types.ObjectId,
      ref: 'offers'
    },
    notify_promo: {
      type: Schema.Types.ObjectId,
      ref: 'promocodes'
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'users'
    },
    number_of_orders: {
      type: Number,
      trim: true
    },
    message: {
      type: String,
      trim: true
    },
    status: {
      type: String,
      default: 'ACTIVE',
      enum: ['ACTIVE', 'INACTIVE']
    },
    sent_date: {
      type: Date,
      trim: true
    },
    schedule_date: {
      type: Date,
      trim: true
    }
  },
  { timestamps: true, versionKey: false }
)

const Notification = mongoose.model('notifications', schema)
module.exports = Notification
