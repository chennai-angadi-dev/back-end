const OrderHistory = require('./schema')
const { errLogger } = require('../../../COMP/api/config/logger')

class Service {
  async createOrderHistory (params) {
    var count = await OrderHistory.countDocuments({ orderId: params.orderId, order_status: params.order_status })
    if (count > 0) {
      return 400
    }
    const data = new OrderHistory(params)
    return (data.save().catch((e) => {
      errLogger.error({ method: 'orderhistory-save', message: e.message })
    }) ? 202 : 599)
  }

  async getAll (req) {
    const reqQuery = req.query
    const limit = Number(
      reqQuery && reqQuery.limit ? reqQuery.limit : process.env.PAGE_LIMIT
    )
    const pageNo = Number(reqQuery && reqQuery.page ? limit * reqQuery.page - limit : 0)
    const sortField = reqQuery && reqQuery.sort ? reqQuery.sort : 'createdAt'
    const order = reqQuery && reqQuery.order && reqQuery.order === 'asc' ? 1 : -1

    const data = await OrderHistory.find()
      .populate({
        path: 'orderId'
      }).populate({
        path: 'orderHistory',
        options: {
          sort: { [`${sortField}`]: order },
          skip: pageNo,
          limit: limit
        }
      })
      .lean().catch((e) => {
        errLogger.error({ method: 'orderHistory-getAll', message: e.message })
      })
    return { data: data, count: data ? data.length : 0 }
  }

  async getById (id) {
    return OrderHistory.findById(id)
      .populate({
        path: 'orderId'
      })
      .lean().catch((e) => {
        errLogger.error({ method: 'OrderHistory-getById', message: e.message })
      })
  }
}

const orderhistoryService = new Service()

module.exports = { Service, orderhistoryService }
