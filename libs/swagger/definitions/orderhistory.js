module.exports = {
  required: [
    'name',
    'categoryId',
    'image'
  ],
  create: {
    required: [
      'orderId',
      'order_status'
    ],
    properties: {
      orderId: {
        type: 'string',
        trim: true
      },
      order_status: {
        type: 'string',
        default: 'ORDER_PLACED',
        enum: ['ORDER_PLACED', 'CONFIRMED', 'ORDER_PROCESSED', 'READY_TO_PICKUP', 'DELIVERY_IN_PROGRESS', 'DELIVERED', 'CANCELLED']
      }
    }
  },
  update: {
    properties: {
      orderId: {
        type: 'string',
        trim: true
      },
      order_status: {
        type: 'string',
        default: 'ORDER_PLACED',
        enum: ['ORDER_PLACED', 'CONFIRMED', 'ORDER_PROCESSED', 'READY_TO_PICKUP', 'DELIVERY_IN_PROGRESS', 'DELIVERED', 'CANCELLED']
      }
    }
  },
  properties: {
    orderId: {
      type: 'string',
      trim: true
    },
    order_status: {
      type: 'string',
      default: 'ORDER_PLACED',
      enum: ['ORDER_PLACED', 'CONFIRMED', 'ORDER_PROCESSED', 'READY_TO_PICKUP', 'DELIVERY_IN_PROGRESS', 'DELIVERED', 'CANCELLED']
    }
  }
}
