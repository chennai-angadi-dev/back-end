const Category = require('./schema')
const { errLogger } = require('../../config/logger')

class Service {
  async createCategory (params) {
    const data = new Category(params)
    return data.save().catch((e) => {
      errLogger.error({ method: 'Category-save', message: e.message })
    })
  }

  getCategoryAndCount (req) {
    const reqQuery = req.query
    const limit = Number(
      reqQuery && reqQuery.limit ? reqQuery.limit : process.env.PAGE_LIMIT
    )
    const pageNo = Number(reqQuery && reqQuery.page ? limit * reqQuery.page - limit : 0)
    const sortField = reqQuery && reqQuery.sort ? reqQuery.sort : 'createdAt'
    const order = reqQuery && reqQuery.order && reqQuery.order === 'asc' ? 1 : -1
    const match = { $and: [] }

    if (reqQuery.status) {
      match.$and.push({ status: reqQuery.status })
    } else {
      match.$and.push({ status: 'ACTIVE' })
    }

    if (reqQuery.name) {
      match.$and.push({ name: { $regex: reqQuery.name, $options: 'i' } })
    }

    return Category.aggregate([
      {
        $match: match
      },
      { $project: { name: 1, status: 1, product: 1, price: 1, image: 1, valid_from: 1, valid_To: 1 } },
      {
        $facet: {
          results: [
            { $sort: { [`${sortField}`]: order } },
            { $skip: pageNo },
            { $limit: limit }
          ],
          totalCount: [
            {
              $count: 'count'
            }
          ]
        }
      }
    ])
  }

  async getAll () {
    return Category.find()
      .select('name status')
      .sort({ createdAt: -1 }).lean().catch((e) => {
        errLogger.error({ method: 'Category-getAll', message: e.message })
      })
  }

  async getById (id) {
    return Category.findById(id)
      .select('name status')
      .lean().catch((e) => {
        errLogger.error({ method: 'Category-getById', message: e.message })
      })
  }

  async updateCategory (id, updateParams) {
    return Category.updateOne(id, updateParams).catch((e) => {
      errLogger.error({ method: 'Category-getById', message: e.message })
    })
  }

  async deleteCategory (id, updateParams) {
    return Category.updateOne(id, updateParams).catch((e) => {
      errLogger.error({ method: 'Category-getById', message: e.message })
    })
  }
}

const categoryService = new Service()

module.exports = { Service, categoryService }
