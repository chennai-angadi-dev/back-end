const _ = require('lodash')
const BaseController = require('../base/controller')
const { BrandService: service } = require('./service')
const { SUCCESS, ERROR } = require('../../libs/constants')
const { apimsgconfig } = require('../../libs/swagger/api_message')

class Controller extends BaseController {
  async createBrand (req, res, next) {
    try {
      const data = await service.createBrand(req.body)
      if (!_.isEmpty(data) && data.id) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[202], code: '202', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[599], code: '599' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getBrands (req, res, next) {
    try {
      const brands = await service.getBrandAndCount(req)
      if (!_.isEmpty(brands) && !_.isEmpty(brands[0].totalCount)) {
        this.sendResponse(req, res, SUCCESS.CODE,
          {
            message: apimsgconfig.en_us[200],
            code: '200',
            data: brands && brands[0].results ? brands[0].results : [],
            count: brands && brands[0].totalCount[0] && brands[0].totalCount[0].count
              ? brands[0].totalCount[0].count
              : 0
          })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getBrandById (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.getById(id)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[200], code: '200', data: result })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async updateBrand (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.updateBrand({ _id: id, status: 'ACTIVE' }, req.body)
      if (!_.isEmpty(result) && result.nModified !== 0) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[203], code: '203', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async deleteBrand (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.deleteBrand({ _id: id }, { status: 'DELETED' })
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en_us[204], code: '204', data: [] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en_us[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }
}

module.exports = Controller
