const mongoose = require('mongoose')

const schema = new mongoose.Schema(
  {
    name: {
      type: String,
      index: true,
      trim: true,
      unique: true,
      required: [true, 'Name is required.']
    },
    status: {
      type: String,
      default: 'ACTIVE',
      enum: ['ACTIVE', 'INACTIVE', 'DELETED ']
    },
    image: {
      type: String,
      trim: true
    }
  },
  { timestamps: true, versionKey: false }
)

const Category = mongoose.model('categories', schema)
module.exports = Category
